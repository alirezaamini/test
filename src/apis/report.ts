import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";

export const GetUserFullDetailsService = () => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/user-full-detail`,
  });
};

export const GetUserTokenFullDetailsService = () => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/token-dashboard/user-full-detail`,
  });
};
export const GetUserTokenSMBPriceChartService = () => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/token-dashboard/smb-price-chart`,
  });
};

export const GetMarketDetailService = () => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/market-detail`,
  });
};

export const GetResourceDetailService = () => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/resource-detail`,
  });
};

export const GetResourceDetailWithDateService = (postData: any) => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/resource-detail-with-date?type=${postData.type}`,
  });
};

export const GetProfitDataService = (postData: any) => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/report/profit-data?type=${postData.type}`,
  });
};



