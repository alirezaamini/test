import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";

export const FetchTransactionHistoryService = (slug: any) => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/transaction/history/current?${slug}`,
  });
};


export const FetchTokenTransactionHistoryService = (slug: any) => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/transaction/token-transaction/history/current?${slug}`,
  });
};

