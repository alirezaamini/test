import React from 'react';
import {IFlag} from "../../layouts/dashboard/Header/Header";

interface IProps {
  item: IFlag,
  onChangeDropDown: any
}

const LangItem = (Props: IProps) => {

  return (
    <div
      onClick={()=>Props.onChangeDropDown(Props.item.link)}
      className="block px-1 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
    >
      <p className="text-center m-0 m-auto">{Props.item.name}</p>
    </div>
  );
};

export default LangItem;
