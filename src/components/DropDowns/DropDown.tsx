import React, {Suspense, useState} from 'react';
import './dropdown.scss';
import LangItem from "./LangItem";
import NotificationItem, {INotification} from "./NotificationItem";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Loading from "../UiKits/Loading/Loading";
import {fetchNotifications} from "../../actions/auth";
import {useTranslation} from "react-i18next";

interface IProps {
    classes: string,
    items: any,
    fetchNotifications?: any,
    onChangeDropDown: any,
    onChangeLang: any,
    kind: string,
    total: number,
    loading: boolean,
}

interface IProfile {
    title: string,
    link: string
}

const DropDown = (Props: IProps) => {
    const [filter, setFilter] = useState<{ take: number, skip: number }>({take: 5, skip: 0})

    const changeLang = (data: string) => {
        //change lang
        if (Props.kind === "languages") {
            Props.onChangeLang(data)
        }
        Props.onChangeDropDown(false)
    };

    const {
        t,
    } = useTranslation();
    const loadMoreNotification = () => {
        setFilter({...filter, skip: filter.skip + filter.take})
        Props.fetchNotifications({...filter, skip: filter.skip + filter.take}, false)
    }
    return (
        <Suspense fallback={<Loading/>}>
            <div className={`
    ${Props.classes}
    ${Props.kind === "languages" && "w-10"}
    ${Props.kind === "profile" && "w-48 profile"}
    ${Props.kind === "notifications" && "w-28rem flex justify-center flex-wrap"}
     menu origin-top-right absolute mt-3
     rounded-md shadow-lg`}>
                <div>
                    <div className="py-1 cursor-pointer" role="menu"
                         aria-orientation="vertical" aria-labelledby="options-menu">

                        {
                            Props.kind === "languages" &&
                            Props.items.map((item: any, index: number) => <LangItem
                                key={index}
                                item={item}
                                onChangeDropDown={(data: string) => changeLang(data)}/>)
                        }

                        {
                            Props.kind === "notifications" &&
                            <div className="p-3 pr-0 notification-container w-24rem">
                                {
                                    Props.items.map((item: INotification, index: number) => <NotificationItem
                                        key={index}
                                        item={item} onChangeDropDown={() => Props.onChangeDropDown(false)}/>)
                                }
                                {
                                    Props.loading &&
                                    <div
                                        className="text-center font-bold block px-4 py-2 text-sm leading-5 text-gray-700 "
                                    > {t('confirmSection.loading')}....</div>
                                }
                                {
                                    !Props.loading && Props.items.length === 0 &&
                                    Props.kind === "notifications" && Props.items.length === 0 &&
                                    <div
                                        className="text-center font-bold block px-4 py-2 text-sm leading-5 text-gray-700 "
                                    > {t('confirmSection.empty')}</div>
                                }
                                {
                                    !Props.loading && Props.items.length > 0 && Props.items.length < Props.total &&
                                    Props.kind === "notifications" && Props.items.length > 0 &&
                                    <div className="w-full">
                                        <button
                                            onClick={() => loadMoreNotification()}
                                            className="button w-full text-white font-bold py-3 px-4 rounded focus:outline-none">
                                            View More
                                        </button>
                                    </div>
                                }
                            </div>
                        }
                        {
                            Props.kind === "profile" &&
                            Props.items.length > 0
                            && Props.items.map((item: IProfile, index: number) => {
                                return (
                                    <>
                                        {
                                            item.link === '/logout'
                                                ? <a href={item.link} key={index}
                                                     onClick={() => Props.onChangeDropDown(false)}
                                                     className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                                                     role="menuitem">{item.title}</a>
                                                : <Link to={item.link} key={index}
                                                        onClick={() => Props.onChangeDropDown(false)}
                                                        className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                                                        role="menuitem">{item.title}</Link>
                                        }
                                    </>
                                )
                            })
                        }

                    </div>
                </div>
            </div>
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        total: state.auth.notifications.total,
        loading: state.auth.notifications.loading,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchNotifications: (data: { take: number, skip: number }, reset: boolean) => dispatch(fetchNotifications(data, reset))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DropDown);
