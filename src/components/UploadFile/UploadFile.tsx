import React from 'react';
// Import React FilePond
import {FilePond, registerPlugin} from 'react-filepond';
// Import FilePond styles
import 'filepond/dist/filepond.min.css';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import './filepond.scss';

// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import {useTranslation} from "react-i18next";

// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview,
    FilePondPluginFileValidateType,
    FilePondPluginFileValidateSize);

interface IProps {
    setFiles: any,
    files: any,
    name: string,
    maxFiles: number,
    optional?: boolean
}

const FileUpload = (Props: IProps) => {

    const {
        t,
    } = useTranslation();
    return (
        <>
            <FilePond
                files={Props.files}
                onupdatefiles={Props.setFiles}
                allowMultiple={Props.maxFiles > 1}
                acceptedFileTypes={['image/*', 'application/pdf']}
                maxFiles={Props.maxFiles}
                className="text-minsk"
                name={Props.name}
                labelIdle={`+ ${t('kyc.choose_file')} ${Props.optional ? t('kyc.optional') : ""}`}
            />
        </>
    );
};

export default FileUpload;
