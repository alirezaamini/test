import React, {Suspense, useEffect} from 'react';
import CountryModel from "../../models/country.model";
import Loading from "../UiKits/Loading/Loading";
import {FetchCountries} from "../../actions/countries";
import {connect} from 'react-redux';

interface IProps {
  kind: string,
  default?: string | number | undefined,
  countries: CountryModel[],
  fetchCountries: any
}

const Countries = (Props: IProps) => {
  useEffect(() => {
    //  get countries
    setTimeout(()=>{
      Props.countries.length === 0 && Props.fetchCountries();
      clearTimeout()
    }, 3000)
  }, [Props]);

  return (
    <Suspense fallback={<Loading/>}>
      <option
          key={12121}
          value={79}>
        Germany
        &nbsp;
        (DE)
      </option>

      {Props.countries.map((item: CountryModel, index: number) => {

          return (
            <option
              selected={item.id === Props.default}
              key={index}
              value={item.id}>
              {item.name}
              &nbsp;
              ({item.code})
            </option>
          )
        }
      )}
    </Suspense>
  );
};

const mapStateToProps = (state: any) => {
  return {
    countries: state.country.countries
  }
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchCountries: () => dispatch(FetchCountries())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Countries);
