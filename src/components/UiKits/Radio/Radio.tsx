import React, {useState} from 'react';
import './radio.scss';

interface Props {
  text: any,
  id: string,
  onChangeStatus: any,
  checked: boolean,
  required: boolean,
}

const Radio = (Props: Props) => {

  const [checkedInput, setChecked] = useState<boolean>(Props.checked);

  const handleChange = () => {
    setChecked(prevState => !prevState);
    Props.onChangeStatus(!checkedInput)
  };

  return (
    <>
      <div className="w-full"/>
      <label className={
        checkedInput
          ? "md:w-2/3 xl:w-full text-left block text-gray-500 font-bold radioLabel active"
          : "md:w-2/3 xl:w-full text-left block text-gray-500 font-bold radioLabel"
      }>
        <input className="mr-2 leading-tight " type="checkbox"
               required={Props.required}
               checked={checkedInput}
               onChange={handleChange}
               id={Props.id} name={Props.id}/>
        <span className="text-sm">
          {Props.text}
          </span>
      </label>
    </>
  );
};

export default Radio;
