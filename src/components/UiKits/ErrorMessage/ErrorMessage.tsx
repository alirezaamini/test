import React from 'react';
import {Link} from 'react-router-dom';

interface IProps {
    src: string,
    title: string,
    body: string,
    buttonTitle: string,
    buttonLink: string,
}

const ErrorMessage = (Props: IProps) => {
    const btnStyle = {
        color: "#fff",
        padding: ".9rem 2.75rem",
        borderRadius: ".25rem",
        fontWeight: 500
    };
    return (
        <div className="mt-10 w-100 error-message h-100 mb-24 bg-white rounded-xl py-16 px-8">
            <div className="error-message-header m-0 m-auto">
                <img src={Props.src} alt={Props.title} className="w-auto h-auto m-0 m-auto"/>
            </div>

            <div className="error-message-body mt-2">
                <p className="m-0 m-auto pb-0 text-minsk text-center font-bold">{Props.title}</p>
            </div>

            <div className="error-message-footer my-10">
                <div className="my-4 flex justify-center">
                    <p className=" sm:w-full md:w-3/4 m-0 m-auto pb-0 text-minsk text-center">{Props.body}</p>
                </div>
                <div className="my-8 m-0 m-auto flex justify-center">
                    <Link to={Props.buttonLink}
                          className="dodgler-button w-56 text-white font-bold py-3 px-4 rounded-xl focus:outline-none text-center"
                          style={btnStyle}>
                        {Props.buttonTitle}
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default ErrorMessage;
