import React from 'react';

export const LoadingStyleNormal = {
  width: '100px',
  height: 'auto',
};

export const LoadingStyleSmall = {
  width: '75px',
  height: 'auto',
};

export const LoadingVeryStyleSmall = {
  width: '25px',
  height: 'auto',
};

const Loading = (Props : any) => {
  const styles = {
    width: "auto",
    height: "auto",
  };

  return (
    <div className="w-full h-full flex justify-center items-center" style={Props.stylesProp2 ? Props.stylesProp2 : null}>
      <img src={"/assets/img/svg/loading.svg"} alt="Loading..." style={Props.stylesProp ? Props.stylesProp : styles}/>
    </div>
  );
};

export default Loading;
