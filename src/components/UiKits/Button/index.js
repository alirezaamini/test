import React from 'react';
import { Btn } from './components';
import { Link } from 'react-router-dom';

const Button = ({
  name = 'Button',
  to = '/',
  backColor = '#3670FB',
  color = 'white',
  model = '',
  styles = {} }) => {
  return <>
    <Btn hasName={name ? 'true' : 'false'}
      data-model={model}
      style={{
        backgroundColor: backColor,
        color: color,
        ...styles
      }}>
      <Link href={to} style={{ color: color }}>
        {name ? <span>{name}</span> : null}
      </Link>
    </Btn>
  </>;
}
export default Button;