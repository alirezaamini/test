import styled from 'styled-components';
interface IProps {
  count: number,
  spaceX: number,
  color: string,
}
export const container = styled.div`
  width: 100%;
  > input {
    width: ${(props: IProps) => `calc(${100 / props.count}% - ${props.spaceX}px)`};
    margin: ${(props: IProps) => `0 ${props.spaceX / 2}px`};
    color: ${(props: IProps) => props.color};
  }  
  float: left;
  height: 5rem;
`;
export const input = styled.input`
  float: left;
  outline: none !important;
  box-shadow: none !important;
  border-bottom: 2px solid;
  height: 40px;
  text-align: center;
  font-size: 16pt;
`;