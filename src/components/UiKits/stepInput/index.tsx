import React from 'react';
import * as C from './styles';
interface IProps {
    count: number,
    spaceX: number,
    color: string,
    onEnd?: any,
}
const StepInput = (Props: IProps) => {
    const [inputs] = React.useState(() => {
        const arr = [];
        for (let i = 0; i < Props.count; i++)
            arr.push(
                <C.input
                    data-index={`step-input-index-${i}`}
                    placeholder=''
                    type='tel'
                    min={0}
                    max={9}
                    onInput={e => handleChange(e)}
                    onKeyDown={e => handleKeyDown(e)}
                    onFocus={e => handleFocus(e)}
                />
            );
        return arr;
    });
    const handleKeyDown = (e: any) => {
        const prev = e.currentTarget.previousSibling,
            next = e.currentTarget.nextSibling;
        switch (e.keyCode) {
            case 8:
            case 46:
                if (prev)
                    setTimeout(() => {
                        prev.select();
                    }, 50);
                break;
            case 38:
            case 39:
                if (next)
                    setTimeout(() => {
                        next.focus();
                    }, 50);
                break;
            case 37:
            case 40:
                if (prev)
                    setTimeout(() => {
                        prev.focus();
                    }, 50);
        }
    }
    const handleFocus = (e: any) => {
        e.currentTarget.select();
        const prev = e.currentTarget.previousSibling;
        if (prev)
            if (!prev.value)
                prev.focus();
    }
    const handleChange = (e: any) => {
        const tar = e.currentTarget,
            val = tar.value,
            next = tar.nextSibling,
            prev = tar.previousSibling;
        if (val) {
            if (val.length > 1 && tar.getAttribute('data-index') === 'step-input-index-0') {
                const splited = val.split('');
                for (let i = 0; i < val.length; i++) {
                    const v = document.querySelector(`input[data-index=step-input-index-${i}]`);
                    const el = v as HTMLInputElement;
                    if (el) {
                        el.value = splited[i];
                        const nextEl = el.nextSibling as HTMLInputElement;
                        if (nextEl) nextEl.focus();
                        else el.focus();
                    }
                }
                if (Props.onEnd) Props.onEnd(val.substr(0, Props.count));
            } else {
                if (next) {
                    next.focus();
                    next.select();
                } else {
                    if (Props.onEnd) {
                        let finalVal = '';
                        for (let i = 0; i < Props.count; i++) {
                            const v = document.querySelector(`input[data-index=step-input-index-${i}]`);
                            const el = v as HTMLInputElement;
                            finalVal += el.value;
                        }
                        Props.onEnd(finalVal);
                    }
                }
            }
            for (let i = 0; i < Props.count; i++) {
                const v = document.querySelector(`input[data-index=step-input-index-${i}]`);
                const el = v as HTMLInputElement;
                if (el.value.length > 1)
                    el.value = el.value.split('').pop() + '';
            }
        } else {
            if (prev) {
                prev.focus();
                prev.select();
            }
        }

    }
    return <>
        <C.container count={Props.count} spaceX={Props.spaceX} color={Props.color}>
            {inputs && inputs.map((item: any, index: number) =>
                <React.Fragment key={index}>{item}</React.Fragment>
            )}
        </C.container>
    </>;
}
export default StepInput;
