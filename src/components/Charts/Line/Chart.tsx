import React, {useRef, useLayoutEffect, Suspense} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Loading from "../../UiKits/Loading/Loading";
import './customChart.scss';

am4core.useTheme(am4themes_animated);

interface IProps {
    data: { date: string, price: number }[],
    candleData: { time: string, open: number, low: number, high: number, close: number }[],
    paddingRight?: any,

}

function CustomChart(Props: IProps) {
    const chart: any = useRef(null);

    useLayoutEffect(() => {
        let x = am4core.create("chartdiv", am4charts.XYChart);
        //=============================configs
        x.dataSource.parser = new am4core.CSVParser();
        // @ts-ignore
        x.dataSource.parser.options.useColumnNames = true;
        // @ts-ignore
        x.dataSource.parser.options.reverse = true;

        // the following line makes value axes to be arranged vertically.
        x.leftAxesContainer.layout = "vertical";
        x.paddingRight = 20;

        //===============dateAxis
        let dateAxis = x.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.renderer.ticks.template.length = 8;
        dateAxis.renderer.ticks.template.strokeOpacity = 0.1;
        dateAxis.renderer.grid.template.disabled = true;
        dateAxis.renderer.ticks.template.disabled = false;
        dateAxis.renderer.ticks.template.strokeOpacity = 0.2;
        dateAxis.renderer.minLabelPosition = 0.01;
        dateAxis.renderer.maxLabelPosition = 0.99;
        dateAxis.keepSelection = true;
        dateAxis.minHeight = 30;
        dateAxis.groupData = true;
        dateAxis.minZoomCount = 5;
        //===============end dateAxis

        //===============valueAxis
        let valueAxis: any = x.yAxes.push(new am4charts.ValueAxis());
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.minWidth = 35;
        valueAxis.tooltip.disabled = true;
        valueAxis.zIndex = 1;
        valueAxis.renderer.baseGrid.disabled = true;
        // height of axis
        valueAxis.height = am4core.percent(65);

        valueAxis.renderer.labels.template.verticalCenter = "bottom";
        valueAxis.renderer.labels.template.padding(2, 2, 2, 2);

        valueAxis.renderer.fontSize = "0.9em";
        //===============end valueAxis


        //===============series
        let series;
        series = x.series.push(new am4charts.LineSeries());
        series.dataFields.dateX = "date";
        series.stroke = am4core.color("#3670fb");
        series.dataFields.valueY = "price";
        series.tooltipText = "{valueY.price}";

        // @ts-ignore
        series.tooltip.background.cornerRadius = 20;
        // @ts-ignore
        series.tooltip.background.fillOpacity = 0.5;
        x.cursor = new am4charts.XYCursor();
        //===============end series

        //===============valueAxis2
        let valueAxis2 = x.yAxes.push(new am4charts.ValueAxis());
// height of axis
        if (window.innerWidth > 585) {
            valueAxis2.height = am4core.percent(20);
        } else {
            valueAxis2.height = am4core.percent(40);
        }
        valueAxis2.zIndex = 3;
// this makes gap between panels
        valueAxis2.marginTop = 60;
        valueAxis2.renderer.baseGrid.disabled = true;
        valueAxis2.renderer.inside = true;
        valueAxis2.renderer.labels.template.verticalCenter = "bottom";
        valueAxis2.renderer.labels.template.padding(2, 2, 2, 2);
        valueAxis2.renderer.fontSize = "0.8em";

        //===============end valueAxis

        //===============series2
        let series2 = x.series.push(new am4charts.ColumnSeries());
        series2.dataFields.dateX = "date";
        series2.dataFields.valueY = "price";
        series2.yAxis = valueAxis2;
        series2.tooltipText = "{valueY.value}";

        series2.name = "MSFT: Volume";
        series2.columns.template.column.fill = am4core.color("#3670fb");
        // volume should be summed
        // @ts-ignore
        series2.tooltip.background.cornerRadius = 20;
        // @ts-ignore
        series2.tooltip.background.fillOpacity = 0.5;
        series2.groupFields.valueY = "sum";
        series2.defaultState.transitionDuration = 0;
        //===============end series2


        chart.current = x;

        //=============================end configs

        //push data
        let dataArray: { date: string, price: number }[] = [];
        Props.data.map((item, index) => dataArray.push({
            date: item.date,
            price: item.price,
        }));

        x.data = dataArray;

        //end push data


        return () => {
            x.dispose();
        };
    }, []);

    useLayoutEffect(() => {
        chart.current.paddingRight = Props.paddingRight;

        let dataArray: { date: string, price: number }[] = [];
        Props.data.map((item, index) => dataArray.push({
            date: item.date,
            price: item.price,
        }));
        chart.current.data = dataArray;

    }, [Props.paddingRight]);


    return (
        <Suspense fallback={<Loading/>}>
            <div className="custom-chart relative">
                <div id="chartdiv" style={{width: "100%", height: "100%"}}/>
            </div>
        </Suspense>
    );
}

export default CustomChart;