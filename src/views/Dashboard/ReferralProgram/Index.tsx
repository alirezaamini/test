import React, {Suspense} from 'react';
import ReferralProgram from '../../../containers/Dashboard/Referral'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <ReferralProgram/>
        </Suspense>
    );
}

export default Index;
