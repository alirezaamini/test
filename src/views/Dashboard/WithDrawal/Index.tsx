import React, {Suspense} from 'react';
import WithDrawal from '../../../containers/Dashboard/WithDrawal'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <WithDrawal/>
        </Suspense>
    );
}

export default Index;
