import {
    GET_MARKET_DETAIL_FAILURE,
    GET_MARKET_DETAIL_REQUEST,
    GET_MARKET_DETAIL_SUCCESS,
    GET_PROFIT_DATA_FAILURE,
    GET_PROFIT_DATA_REQUEST,
    GET_PROFIT_DATA_SUCCESS,
    GET_RESOURCE_DETAIL_FAILURE,
    GET_RESOURCE_DETAIL_REQUEST,
    GET_RESOURCE_DETAIL_SUCCESS,
    GET_RESOURCE_DETAIL_WITH_DATE_FAILURE,
    GET_RESOURCE_DETAIL_WITH_DATE_REQUEST,
    GET_RESOURCE_DETAIL_WITH_DATE_SUCCESS,
    GET_USER_FULL_DETAILS_FAILURE,
    GET_USER_FULL_DETAILS_REQUEST,
    GET_USER_FULL_DETAILS_SUCCESS,
    GET_USER_TOKEN_FULL_DETAILS_FAILURE,
    GET_USER_TOKEN_FULL_DETAILS_REQUEST,
    GET_USER_TOKEN_FULL_DETAILS_SUCCESS,
    GET_USER_TOKEN_SMB_PRICE_CHART_FAILURE,
    GET_USER_TOKEN_SMB_PRICE_CHART_REQUEST,
    GET_USER_TOKEN_SMB_PRICE_CHART_SUCCESS
} from '../constants/actionTypes';
import {ReportActionTypes} from "../actions/report";

export interface DefaultStateI {
    userFullDetails: {
        loading: boolean,
        data: any,
        error: string | null
    },
    marketDetail: {
        loading: boolean,
        data: any,
        error: string | null
    },
    resourceDetail: {
        loading: boolean,
        data: any,
        error: string | null
    },
    resourceDetailWithDate: {
        loading: boolean,
        data: any,
        daily: {
            result: any[]
        },
        weekly: {
            result: any[]
        },
        monthly: {
            result: any[]
        },
        yearly: {
            result: any[]
        },
        error: string | null
    },
    dailyProfitData: {
        loading: boolean,
        data: any,
        error: string | null
    },
    profitData: {
        loading: boolean,
        data: any,
        error: string | null
    },

//  user token
    userTokenFullDetails: {
        loading: boolean,
        data: any,
        error: string | null
    },
    userTokenSMBPriceChart: {
        loading: boolean,
        data: { date: string, value: number }[],
        error: string | null
    },

}

const defaultState: DefaultStateI = {
    userFullDetails: {
        loading: false,
        data: null,
        error: null
    },
    marketDetail: {
        loading: false,
        data: null,
        error: null
    },
    resourceDetail: {
        loading: false,
        data: [],
        error: null
    },
    resourceDetailWithDate: {
        loading: false,
        data: null,
        daily: {
            result: []
        },
        weekly: {
            result: []
        },
        monthly: {
            result: []
        },
        yearly: {
            result: []
        },
        error: null
    },
    dailyProfitData: {
        loading: false,
        data: [],
        error: null
    },
    profitData: {
        loading: false,
        data: [],
        error: null
    },

    //  user token
    userTokenFullDetails: {
        loading: false,
        data: null,
        error: null
    },
    userTokenSMBPriceChart: {
        loading: false,
        data: [],
        error: null
    },
};


const reportReducer = (state: DefaultStateI = defaultState,
                       action: ReportActionTypes): DefaultStateI => {
    switch (action.type) {
        case GET_USER_FULL_DETAILS_REQUEST:
            return {
                ...state,
                userFullDetails: {
                    ...state.userFullDetails,
                    loading: true,
                    error: null
                }

            }
        case GET_USER_FULL_DETAILS_SUCCESS:
            return {
                ...state,
                userFullDetails: {
                    ...state.userFullDetails,
                    ...action.payload,
                    error: null,
                    loading: false,
                }
            }
        case GET_USER_FULL_DETAILS_FAILURE:
            return {
                ...state,
                userFullDetails: {
                    ...state.userFullDetails,
                    loading: false,
                    data: null,
                    error: action.payload
                }

            }

        case GET_MARKET_DETAIL_REQUEST:
            return {
                ...state,
                marketDetail: {
                    ...state.marketDetail,
                    loading: true,
                    error: null
                }

            }
        case GET_MARKET_DETAIL_SUCCESS:
            return {
                ...state,
                marketDetail: {
                    ...state.marketDetail,
                    ...action.payload,
                    error: null,
                    loading: false,
                }
            }
        case GET_MARKET_DETAIL_FAILURE:
            return {
                ...state,
                marketDetail: {
                    ...state.marketDetail,
                    loading: false,
                    data: null,
                    error: action.payload
                }

            }

        case GET_RESOURCE_DETAIL_REQUEST:
            return {
                ...state,
                resourceDetail: {
                    ...state.resourceDetail,
                    loading: true,
                    error: null
                }

            }
        case GET_RESOURCE_DETAIL_SUCCESS:
            return {
                ...state,
                resourceDetail: {
                    ...state.resourceDetail,
                    data: action.payload,
                    error: null,
                    loading: false,
                }
            }
        case GET_RESOURCE_DETAIL_FAILURE:
            return {
                ...state,
                resourceDetail: {
                    ...state.resourceDetail,
                    loading: false,
                    data: null,
                    error: action.payload
                }

            }

        case GET_RESOURCE_DETAIL_WITH_DATE_REQUEST:
            return {
                ...state,
                resourceDetailWithDate: {
                    ...state.resourceDetailWithDate,
                    loading: true,
                    error: null
                }

            }
        case GET_RESOURCE_DETAIL_WITH_DATE_SUCCESS:
            return {
                ...state,
                resourceDetailWithDate: {
                    ...state.resourceDetailWithDate,
                    data: action.payload.data,
                    daily: action.payload.type === "Daily" ? action.payload.data : state.resourceDetailWithDate.daily,
                    weekly: action.payload.type === "Weekly" ? action.payload.data : state.resourceDetailWithDate.weekly,
                    monthly: action.payload.type === "Monthly" ? action.payload.data : state.resourceDetailWithDate.monthly,
                    yearly: action.payload.type === "Yearly" ? action.payload.data : state.resourceDetailWithDate.yearly,
                    error: null,
                    loading: false,
                }
            }
        case GET_RESOURCE_DETAIL_WITH_DATE_FAILURE:
            return {
                ...state,
                resourceDetailWithDate: {
                    ...state.resourceDetailWithDate,
                    loading: false,
                    error: action.payload
                }

            }

        case GET_PROFIT_DATA_REQUEST:
            return {
                ...state,
                profitData: {
                    ...state.profitData,
                    loading: true,
                    error: null
                }

            }
        case GET_PROFIT_DATA_SUCCESS:
            return {
                ...state,
                profitData: {
                    ...state.profitData,
                    data: action.payload.data,
                    error: null,
                    loading: false,
                },
                dailyProfitData: {
                    ...state.dailyProfitData,
                    data: action.payload.type === "Daily" ? action.payload.data : state.dailyProfitData.data,
                    error: null,
                    loading: false,
                }
            }
        case GET_PROFIT_DATA_FAILURE:
            return {
                ...state,
                profitData: {
                    ...state.profitData,
                    loading: false,
                    data: null,
                    error: action.payload
                }

            }

        case GET_USER_TOKEN_FULL_DETAILS_REQUEST:
            return {
                ...state,
                userTokenFullDetails: {
                    ...state.userTokenFullDetails,
                    loading: true,
                    error: null
                }

            }
        case GET_USER_TOKEN_FULL_DETAILS_SUCCESS:
            return {
                ...state,
                userTokenFullDetails: {
                    ...state.userTokenFullDetails,
                    ...action.payload,
                    error: null,
                    loading: false,
                }
            }
        case GET_USER_TOKEN_FULL_DETAILS_FAILURE:
            return {
                ...state,
                userTokenFullDetails: {
                    ...state.userTokenFullDetails,
                    loading: false,
                    data: null,
                    error: action.payload
                }

            }

        case GET_USER_TOKEN_SMB_PRICE_CHART_REQUEST:
            return {
                ...state,
                userTokenSMBPriceChart: {
                    ...state.userTokenSMBPriceChart,
                    loading: true,
                    error: null
                }

            }
        case GET_USER_TOKEN_SMB_PRICE_CHART_SUCCESS:
            return {
                ...state,
                userTokenSMBPriceChart: {
                    ...state.userTokenSMBPriceChart,
                    data: action.payload.items,
                    error: null,
                    loading: false,
                }
            }
        case GET_USER_TOKEN_SMB_PRICE_CHART_FAILURE:
            return {
                ...state,
                userTokenSMBPriceChart: {
                    ...state.userTokenSMBPriceChart,
                    loading: false,
                    data: [],
                    error: action.payload
                }

            }

        default:
            return state
    }
}

export default reportReducer;