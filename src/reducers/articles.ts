import {
  IArticles,
  ArticlesTypes,
} from "../actions/articles";

import {
  SET_ARTICLES, SET_ARTICLES_FAILURE
} from '../constants/actionTypes';


export interface ArticlesDefaultStateI {
  items: IArticles[],
  error: string
}

const defaultState: ArticlesDefaultStateI = {
  items: [],
  error: ''
};

const setArticles = (state: ArticlesDefaultStateI, action: IArticles[]) => {
  return {
    items: action,
    error: ''
  }
};


 const articles = (state: ArticlesDefaultStateI = defaultState,
                         action: ArticlesTypes): ArticlesDefaultStateI => {
  switch (action.type) {
    case SET_ARTICLES:
      return setArticles(state, action.payload.articles)
    case SET_ARTICLES_FAILURE:
      return {
        ...state,
        error: action.payload.error
      };
    default:
      return state
  }
};

export default articles;