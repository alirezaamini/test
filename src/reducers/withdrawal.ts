import {
  IWithdrawalRequests,

  WithdrawalsTypes
} from "../actions/withdrawal";

import {
  SEND_WITHDRAWAL_FAILURE,
  SEND_WITHDRAWAL_REQUEST,
  SEND_WITHDRAWAL_SUCCESS,
  SET_WITHDRAWALS,
  SET_WITHDRAWALS_FAILURE,
  SET_WITHDRAWALS_REQUEST,
} from '../constants/actionTypes';

export interface WithdrawalRequestsDefaultStateI {
  items: IWithdrawalRequests[],
  total: number,
  loading: boolean,
  reset: boolean,
  error: string,
  request: {
    status: string,
    loading: boolean,
    error: string,
  }
}

const defaultState: WithdrawalRequestsDefaultStateI = {
  items: [],
  total: 0,
  loading: false,
  reset: false,
  error: '',
  request: {
    status: '',
    loading: false,
    error: '',
  }
};

const setCurrentWithdrawals = (state: WithdrawalRequestsDefaultStateI, action: WithdrawalRequestsDefaultStateI) => {

  if (action.reset) {
    return {
      ...state,
      items: action.items,
      total: action.total,
      error: '',
      loading: false
    }
  } else {
    return {
      ...state,
      items: [...action.items, ...state.items],
      total: state.total + action.items.length,
      error: '',
      loading: false
    }
  }

};


 const withdrawalsReducer = (state: WithdrawalRequestsDefaultStateI = defaultState, action: WithdrawalsTypes): WithdrawalRequestsDefaultStateI => {
  switch (action.type) {
    case SET_WITHDRAWALS_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case SET_WITHDRAWALS:
      return setCurrentWithdrawals(state, {
        ...state,
        items: action.payload.items,
        total: action.payload.total,
        reset: action.payload.reset
      });
    case SET_WITHDRAWALS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
      };
    case SEND_WITHDRAWAL_REQUEST:
      return {
        ...state,
        request: {
          ...state.request,
          loading: true,
          error: '',
          status: '',
        }
      };
    case SEND_WITHDRAWAL_FAILURE:
      return {
        ...state,
        request: {
          ...state.request,
          error: action.payload.error,
          loading: false,
        }
      };
    case SEND_WITHDRAWAL_SUCCESS:
      return {
        ...state,
        request: {
          ...state.request,
          status: action.payload.status,
          loading: false,
          error: '',
        }
      };
    default:
      return state
  }
};
export default withdrawalsReducer