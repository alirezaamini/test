export default interface MembershipModel {
  id: string;
  slug: string;
  title: string;
}