import UserModel from "./user.model";

export default interface WithdrawalModel {
  id: number,
  trackingCode: string,
  amount: number,
  user: UserModel,
  paidAt: string,
  createdAt: string,
  status: string,
  isActive: boolean
}
