export default interface UserPhoto {
  path?: string,
  name?: string,
}
