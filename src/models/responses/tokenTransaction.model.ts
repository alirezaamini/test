import UserModel from "../user.model";

export interface TokenTransactionModel {
    id: number,
    user: UserModel,
    operator: UserModel,
    token_amount: 0,
    price_amount: 0,
    isManually: true,
    createdAt: string,
    status: string,
    type: string,
    extra: string,
}
