
export default interface UserAgreementChildModel {
  isAbove18: boolean,
  imNotUsCitizen: boolean,
}
