export interface ArbitrageGraph {
    data: {
        id: number,
        name: string,
        symbol: string,
        quotes: {
            time_open: string,
            time_close: string,
            time_high: string,
            time_low: string,
            quote: {
                open: string,
                high: string,
                low: string,
                close: string,
                volume: string,
                market_cap: string,
                timestamp: string,
            }
        }[],
    },
    status: {
        timestamp: string,
        error_code: number,
        error_message: string,
        elapsed: number,
        credit_count: number,
    }
}


export const ArbitrageGraphDefaultState = {

};
