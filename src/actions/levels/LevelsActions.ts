import {Dispatch} from "redux";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {levelInterface} from "../../models/levels.model";
import {SET_LEVELS, SET_LEVELS_FAILURE, SET_LEVELS_REQUEST} from "../../constants/actionTypes";
import {FetchLevelsService} from "../../apis/levels";

export type LevelsTypes = SetLevels | SetLevelsRequest | SetLevelsFailure

export const FetchLevels = () => async (dispatch: Dispatch<LevelsTypes>) => {
  try {
    dispatch({type: SET_LEVELS_REQUEST});
    //  fetch levels
    const res = await FetchLevelsService();
    if (res.status === 200 || res.status === 201) {
      //set to reducer
      dispatch({
        type: SET_LEVELS,
        payload: JSON.parse(Decrypt(res.data.data))
      })
    } else if (res.status === 401) {
      dispatch({
        type: SET_LEVELS_FAILURE,
        payload: {
          error: 'Unauthorized'
        }
      })
    } else {
      let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
      dispatch({
        type: SET_LEVELS_FAILURE,
        payload: {
          error: err
        }
      })
    }

  } catch (e) {
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_LEVELS_FAILURE,
      payload: {
        error: err
      }
    })
  }
};


//interfaces

interface SetLevels {
  type: typeof SET_LEVELS,
  payload: levelInterface[]
}


interface SetLevelsRequest {
  type: typeof SET_LEVELS_REQUEST,

}


interface SetLevelsFailure {
  type: typeof SET_LEVELS_FAILURE,
  payload: { error: string }
}
