export default {
  // app_site_URL: 'http://localhost:3000',
  // app_site_URL: 'https://test2.smartbitrage.com',
  // base_URL: 'https://app.smartbitrage.com',
  // api_base_URL: 'https://sandbox.smartbitrage.com/api',
  app_site_URL: 'https://app.pntcap.com',
  base_URL: 'https://api.pntcap.com',
  api_base_URL: 'https://api.pntcap.com/api',

  //services
  coin_market_cap_api_key: 'd9741fa7-93ef-4966-8bff-4fed3f4205b6',

//  apis
//  auth
  api_update_firebase_token: "/auth/update/firebase-token",
  //kyc
  api_kyc_request_upload: "/auth/kyc-request/upload",
//global
  api_upload: "/auth/upload",
  //market
  api_live_arbitrage_graph: "https://pro-api.coinmarketcap.com/v1/cryptocurrency/ohlcv/historical",
  //withdrawal
  api_withdrawal_request: "/withdrawal/request",
  //invest
  api_invest_request: "/deposit/request",
  api_invest_upload: "/deposit/request/upload",
  //articles
  api_get_articles: "https://pntcap.com/wp-json/wp/v2/presses-cat",

//  routes
  auth: "/auth",
  home: "/home",
  login: '/auth/login',
  logout: '/logout',
  register: "/auth/register",
  resetPassword: "/auth/resetPassword",
  verify: "/auth/verify",
  forgotPassword: '/auth/forgotPassword',
  deepLinks: '/dl',

  dashboard: "/dashboard",
  dashboard_home: "/dashboard/home",
  dashboard_invest: "/dashboard/invest",
  dashboard_investment_management: "/dashboard/investment-management",
  dashboard_live_arbitrage: "/dashboard/live-arbitrage",
  dashboard_accounts: "/dashboard/accounts",
  dashboard_referral_program: "/dashboard/referral-program",
  dashboard_transaction_history: "/dashboard/transaction-history",
  dashboard_withdrawal: "/dashboard/withdrawal",
  dashboard_kyc: "/dashboard/kyc",
  dashboard_account_details: "/dashboard/account-details",
  dashboard_settings: "/dashboard/settings",
  dashboard_documents: "/dashboard/documents",
  dashboard_faq: "/dashboard/faq",
  dashboard_get_premium: "/dashboard/get-premium",
  dashboard_calculator: "/dashboard/calculator",

  token_dashboard: "/token-dashboard",
  token_dashboard_home: "/token-dashboard/home",
  token_dashboard_referral_program: "/token-dashboard/referral-program",
  token_dashboard_transaction_history: "/token-dashboard/transaction-history",
  token_dashboard_kyc: "/token-dashboard/kyc",
  token_dashboard_account_details: "/token-dashboard/account-details",
  token_dashboard_settings: "/token-dashboard/settings",
  token_dashboard_faq: "/token-dashboard/faq",
  token_dashboard_get_premium: "/token-dashboard/get-premium",
}
