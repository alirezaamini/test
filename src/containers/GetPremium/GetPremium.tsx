import React from 'react';
import ErrorMessage from "../../components/UiKits/ErrorMessage/ErrorMessage";
import URLs from "../../constants/URLs";
import {useTranslation} from "react-i18next";

const My404Com = () => {

    const {
        t,
    } = useTranslation();
    return (
        <div className="w-100 h-auto">
            <ErrorMessage src="/assets/img/png/premium-min.png" title={t('premium.title')}
                          body={t('premium.description')}
                          buttonTitle={t('sidebar.invest')} buttonLink={URLs.dashboard_invest}/>
        </div>
    );
};

export default My404Com;
