import React from 'react';
import './InfoCart.scss';

interface IProps {
  icon: string,
  title: string,
  value: string
}

const InfoCart = (Props: IProps) => {

  return (
    <div className="info-cart flex flex-wrap items-stretch mt-2 lg:mt-4 mx-2">
      <div className="w-full flex flex-nowrap justify-start items-center h-full">
        <div className="image flex items-center justify-center">
          <img src={Props.icon} alt=""/>
        </div>

        <div className="ml-6 pl-5 border-gray-300 border-l-2">
          <div className="text text-minsk ">{Props.title}</div>
          <div className="font-bold body"><p className="rtl text-left">{Props.value}</p></div>
        </div>
      </div>
    </div>
  );
};

export default InfoCart;
