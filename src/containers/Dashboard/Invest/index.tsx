import React, {ChangeEvent, Suspense, useEffect, useState} from 'react';
import Loading from "../../../components/UiKits/Loading/Loading";
import '../WithDrawal/withdrawal.scss';
import './invest.scss';
import {connect} from 'react-redux';
import {filterI} from "../WithDrawal";
import {
    activeInvestDefaultState,
    clearInvestStates,
    FetchInvests,
    IInvestRequests, investRequest, sendInvestRequest, uploadInvestDocument,
} from "../../../actions/invest";
import ClassicTable, {thead} from "../../../components/UiKits/ClassicTable/ClassicTable";
import ReactPaginate from "react-paginate";
import {NextButton, PrevButton} from "../../../components/UiKits/Pagination/Pagination";
import InvestRows from "./components/InvestRows";
import InvestModel from "../../../models/invest.model";
import 'react-image-lightbox/style.css';
import ConfirmModal from "../../../components/UiKits/Modal/ConfirmModal";
import {useTranslation} from "react-i18next";
import UserModel from "../../../models/user.model";
import {LevelItemInterface} from "../../../models/levels.model";
import {FetchLevels} from "../../../actions/levels/LevelsActions";
import InvestStep2 from "./components/step2/InvestStep2";
import InvestStep3 from "./components/step3/InvestStep3";
import InvestStep1 from "./components/step1/InvestStep1";
import InvestStepTitle from "./components/InvestStepTitle";
import {CHANGE_INVEST_STEP} from "../../../constants/actionTypes";

interface IProps {
    balance: string,
    sendInvestRequest: any,
    sendInvestSerial: any,
    fetchData: any,
    clearStatus: any,
    investError: string,
    investStatus: string,
    investLoading: boolean,
    hasInvest: boolean,
    tableError: string,
    tableLoading: boolean,
    total: number,
    allData: InvestModel[],
    levelsLength: number,
    stepNumber: number,
    user: UserModel,
    fetchLevels: any,
    setStepNumber: any,
    level: LevelItemInterface,
    activeInvest: IInvestRequests,
    uploadError: string,
    uploadLoading: boolean,
    uploadStatus: string,

}

export interface FileI {
    format: string,
    file: File | undefined,
    src: any
}

export interface investLocalStorage {
    amount: string,
    reference: string,
    investId: string,
}

const Index = (Props: IProps) => {
    const [filter, setFilter] = useState<filterI>({take: 5, skip: 0});
    const [document, setDocument] = useState<FileI | null>(null);
    const [amount, setAmount] = useState<number | null>(null);
    const [show, setShow] = useState<boolean>(false);
    const [modalStep1, setModalStep1] = useState<boolean>(false);
    const [status, setStatus] = useState<string | null>(null);

    useEffect(() => {
        clearInterval();
        if (Props.activeInvest.lastActivity) {
            setStatus(Props.activeInvest.lastActivity.status)
        } else {
            setStatus('Disable')
        }
        Props.levelsLength === 0 && Props.fetchLevels();
        Props.fetchData(filter);
        let interval: any;
        if (Props.total < 5) {
            interval = setInterval(function () {
                setFilter({take: 5, skip: 0});
                Props.fetchData({take: 5, skip: 0});
                Props.fetchData(filter)
            }, 1000 * 60);
        }

        return function cleanup() {
            clearInterval(interval);
            Props.clearStatus();
        };
    }, []);

    useEffect(() => {
        if (Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status !== status) {
            if (Props.activeInvest.lastActivity) {
                Props.activeInvest.lastActivity.status === 'Disable' && Props.setStepNumber(1);
                Props.activeInvest.lastActivity.status === 'InCompleted' && Props.setStepNumber(2);
                Props.activeInvest.lastActivity.status === 'Pending' && Props.setStepNumber(3);
                Props.activeInvest.lastActivity.status === 'Rejected' && Props.setStepNumber(3);
                setStatus(Props.activeInvest.lastActivity.status);
            } else {
                Props.setStepNumber(1);
            }
        }
    }, [Props.activeInvest.lastActivity]);

    useEffect(() => {
        if (Props.uploadStatus.includes('Waiting') || Props.uploadError !== '' || Props.uploadStatus.includes('Success')) {
            window.scrollTo(0, 100);
            setShow(false);
            if (Props.uploadStatus.includes('Success')) {
                setDocument(null);
                setFilter({take: 5, skip: 0});
                Props.fetchData({take: 5, skip: 0});
                Props.clearStatus();
            }
        }
    }, [Props.uploadStatus || Props.uploadError]);

    useEffect(() => {
        if (Props.investError !== '' || Props.investStatus.includes('Success')) {
            window.scrollTo(0, 100);
            setModalStep1(false);
        }
    }, [Props.investStatus || Props.investError]);

    const sendInvestSerialConfirm = () => Props.sendInvestSerial(Props.activeInvest.id, document);

    const [activePage, setActivePage] = useState<number>(0);
    const onPageChange = (number: number) => {
        setActivePage(number);
        setFilter({...filter, skip: number * filter.take});
        Props.fetchData({...filter, skip: number * filter.take});

    };

    const handleChangeDocument = (e: ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files ? e.target.files[0] : undefined;
        const format = e.target.files ? e.target.files[0].type : "";
        setDocument({file: file, format: format, src: URL.createObjectURL(file)});
    };

    //step 1
    const sendInvestRequest = () => {

        let data: investRequest = {
            amount: amount || 0,
            description: ""
        };

        Props.sendInvestRequest(data, true);
    };

    const {t} = useTranslation();

    const headers: thead[] = [
        {title: 'ID'},
        {title: t('invest.date')},
        {title: t('invest.amount')},
        {title: t('invest.amount_accepted')},
        {title: t('invest.status')},
        {title: t('invest.tracking_code')},
        {title: t('invest.document')},
    ];

    return (
        <div className="faq mt-10 md:mt-8 w-full invest">
            {
                show
                &&
                <ConfirmModal callBackConfirm={() => sendInvestSerialConfirm()}
                              callBackCancel={() => setShow(false)}
                              callBackClose={() => setShow(false)} title={t('accounts.invest')}
                              loading={Props.investLoading || Props.uploadLoading}
                              kind={"blue"}
                              content={t('confirmSection.description')}
                              icon={""}/>
            }
            {
                modalStep1
                &&
                <ConfirmModal callBackConfirm={() => sendInvestRequest()}
                              callBackCancel={() => setModalStep1(false)}
                              callBackClose={() => setModalStep1(false)} title={t('accounts.invest')}
                              loading={Props.investLoading || Props.uploadLoading}
                              kind={"blue"}
                              content={t('confirmSection.description')}
                              icon={""}/>
            }

            <div className="faq-container rounded-xl pt-2 pb-8 md:pb-10 px-4 md:px-0 w-full bg-white border-xl">

                {Props.stepNumber === 1 &&
                <InvestStepTitle title={t('invest.plans')}
                                 description={t('invest.Enjoy_the_smartbitrage_unique_features_has_to_offer')}/>}
                {Props.stepNumber === 2 &&
                <InvestStepTitle title={t('invest.bank_information')}
                                 description={t('invest.Enjoy_the_smartbitrage_unique_features_has_to_offer')}/>}
                {Props.stepNumber === 3 && Props.activeInvest.lastActivity.status !== 'Pending' &&
                <InvestStepTitle title={t('invest.upload_documents')}
                                 description={t('invest.Enjoy_the_smartbitrage_unique_features_has_to_offer')}/>
                }

                <div className="w-full md:w-3/4 px-0 md:px-12 my-2 md:my-4 mx-auto">
                    <div className="w-full mx-auto text-minsk my-1 md:my-0">
                        <div className=" invest-steps overflow-auto without-scroll-bar justify-start lg:justify-center">
                            <div className={`invest-step-item ${Props.stepNumber === 1 ? 'active' : "inactive"}`}>
                                <div className="ml-8 "
                                     onClick={() => Props.setStepNumber(1)}>
                                    <div className="font-thin text-sm">Step 1</div>
                                    <div className="text-xs">Choose your plan</div>
                                </div>
                            </div>
                            <div className={`invest-step-item ${Props.stepNumber === 2 ? 'active' : "inactive"}`}>
                                <div className="ml-8 "
                                     onClick={() => Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status !== "Disable" && Props.setStepNumber(2)}>
                                    <div className="font-thin text-sm">Step 2</div>
                                    <div className="text-xs">Get bank info</div>
                                </div>
                            </div>
                            <div className={`invest-step-item ${Props.stepNumber === 3 ? 'active' : "inactive"}`}>
                                <div className="ml-8 "
                                     onClick={() => Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status !== "Disable" && Props.setStepNumber(3)}>
                                    <div className="font-thin text-sm">Step 3</div>
                                    <div className="text-xs">Upload bank receipt</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {Props.stepNumber === 1 &&
                <InvestStep1 setShow={(show: boolean) => setModalStep1(show)}
                             onChangeAmount={(amount: number | null) => setAmount(amount)}
                             onSetStepNumber={(number: number) => Props.setStepNumber(number)}/>}

                {Props.stepNumber === 2 &&
                <InvestStep2 onSetStepNumber={(number: number) => Props.setStepNumber(number)}/>}

                {Props.stepNumber === 3 &&
                <InvestStep3 show={show} setShow={(show: boolean) => setShow(show)} document={document}
                             onSetStepNumber={(number: number) => Props.setStepNumber(number)}
                             setDocument={(data: any) => setDocument(data)}
                             handleChangeDocument={(data: any) => handleChangeDocument(data)}
                             uploadError={Props.uploadError} uploadStatus={Props.uploadStatus}
                             activeInvest={Props.activeInvest}
                             uploadLoading={Props.uploadLoading}/>
                }

            </div>

            <div className="transaction-history live-arbitrage">
                <div className="my-20 pb-5 md:pb-10 pt-5 md:pt-10 w-full bg-white rounded-xl">
                    <div className="ml-5 md:ml-6 my-3 flex flex-wrap justify-between items-center">
                        <div
                            className="pl-1 home-title text-normal text-minsk my-3">{t('invest.invest_history')}</div>
                    </div>
                    <ClassicTable error={Props.tableError} headers={headers}
                                  classes={" huge "}
                                  loading={Props.tableLoading} total={Props.total}
                                  children={<InvestRows data={Props.allData}/>}/>

                    {
                        Props.total > 5
                        &&
                        <div className="pagination sm:flex-1 sm:flex sm:items-center
                                        sm:justify-center bg-white h-20 rounded-bl-lg rounded-br-lg">
                            <ReactPaginate
                                previousLabel={PrevButton}
                                nextLabel={NextButton}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={Props.total / filter.take}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={(number) => onPageChange(number.selected)}
                                forcePage={activePage}
                                containerClassName={'pagination'}
                                // subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                            />
                        </div>
                    }
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state: any) => {
    return {
        balance: state.auth.user.balance,
        investError: state.invest.request.error,
        investLoading: state.invest.request.loading,
        investStatus: state.invest.request.status,
        uploadError: state.invest.upload.error,
        uploadLoading: state.invest.upload.loading,
        uploadStatus: state.invest.upload.status,
        allData: state.invest.items,
        total: state.invest.total,
        tableLoading: state.invest.loading,
        tableError: state.invest.error,
        stepNumber: state.invest.stepNumber,
        levelsLength: state.levels.items ? state.levels.items.length : 0,
        user: state.auth.user,
        level: state.auth.level,
        hasInvest: state.auth.hasInvest,
        activeInvest: state.invest.activeInvest ? state.invest.activeInvest : activeInvestDefaultState,

    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        sendInvestSerial: (id: number, file: FileI) => dispatch(uploadInvestDocument(id, file)),
        sendInvestRequest: (data: investRequest, reset: boolean) => dispatch(sendInvestRequest(data, reset)),
        fetchData: (data: filterI) => dispatch(FetchInvests(data)),
        fetchLevels: () => dispatch(FetchLevels()),
        setStepNumber: (number: number) => dispatch({
            type: CHANGE_INVEST_STEP,
            payload: {
                stepNumber: number
            }
        }),
        clearStatus: () => dispatch(clearInvestStates()),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Index);
