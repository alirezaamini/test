import React, {Suspense, useEffect, useState} from 'react';
import './home.scss';
import {Link} from 'react-router-dom';
import URLs from "../../../constants/URLs";
import Loading from "../../../components/UiKits/Loading/Loading";
import {connect} from 'react-redux';
import {useTranslation} from "react-i18next";
import Cart from "./Components/Cart";
import InfoCart from "./Components/InfoCart";
import CartChart from './Components/CartChart/CartChart';
import GridRow from "./Components/GridRow";
import BarChart from "../../../components/Charts/Bar";
import ProfitChart from "../../../components/Charts/Line/ProfitChart";
import ProfitBarChart from "../../../components/Charts/Line/ProfitBarChart";
import {
    GetMarketDetail, GetProfitData,
    GetResourceDetail,
    GetResourceDetailWithDate,
    GetUserFullDetails
} from "../../../actions/report";
import {getLocalStorage} from "../../../assets/helpers/storage";
import UserFullDetailsModel from "../../../models/userFullDetails.model";
import MarketModel from "../../../models/market.model";
import ResourceModel from "../../../models/resource.model";
import '../../../components/Charts/Donut/donut-chart.scss';
import ProfitCard from './Components2/profitCard';
import TotalDeposit from './Components2/totalDeposit';
import DailyProfitCard from './Components2/dailyProfitCard';
import {currencyFormatter, readableNumber} from "../../../assets/helpers/utilities";
import DonutChart from "../../../components/Charts/Donut";
import Tab from "./Components/Tab";

const Articles = React.lazy(() => import("../../../components/Articles/Articles"));

interface IProps {
    getDailyProfitData: any[],

    articles: any,

    GetProfitData: any,
    getProfitData: any,
    getProfitLoading: boolean,
    getProfitError: string | null,

    GetResourceDetailWithDate: any,
    getResourceDetailWithDateData: any,
    getResourceDetailWithDateDaily: any,
    getResourceDetailWithDateWeekly: any,
    getResourceDetailWithDateMonthly: any,
    getResourceDetailWithDateYearly: any,
    getResourceDetailWithDateLoading: boolean,
    getResourceDetailWithDateError: string | null,

    GetResourceDetail: any,
    getResourceDetailData: ResourceModel[],
    getResourceDetailLoading: boolean,
    getResourceDetailError: string | null,

    GetMarketDetail: any,
    getMarketDetailLoading: boolean,
    getMarketDetailError: string | null,

    GetUserFullDetails: any,
    getUserDetailsLoading: boolean,
    getUserDetailsError: string | null,
}

const Index = (Props: IProps) => {
    const userFullDetails: UserFullDetailsModel | null = getLocalStorage('userFullDetails');
    const marketDetail: MarketModel | null = getLocalStorage('marketDetail');
    const [profitDataFilter, setProfitDataFilter] = useState({type: 'Daily'})
    const [resourceDetailWithDate, setResourceDetailWithDate] = useState<string>("percent")
    const [resourceDetail, setResourceDetail] = useState<string>("percent")

    useEffect(() => {
        Props.GetUserFullDetails();
        Props.GetMarketDetail();
        Props.GetResourceDetail();
    }, [])

    useEffect(() => {
        Props.GetResourceDetailWithDate(profitDataFilter);
        Props.GetProfitData(profitDataFilter);
    }, [profitDataFilter])

    const {t} = useTranslation();

    return (
        <div className="home mt-2 md:mt-8">
            <div className="grid grid-cols-1 md:grid-cols-12 ">
                <div className="col-span-4 flex flex-wrap justify-center items-baseline pr-0 md:pr-3 mb-4 md:mb-0 ">
                        <div className="w-full flex justify-center items-baseline">
                            <div className="w-full">
                                <ProfitCard classes="bg-green-500"
                                            chart={Props.getDailyProfitData.map(item => parseFloat(item.amount).toFixed(2))}
                                            title={t('home.total_profit')}
                                            value={userFullDetails ? currencyFormatter(userFullDetails.financial.totalProfit) : "-"}
                                            roi={
                                                userFullDetails
                                                    ? userFullDetails.financial.roiPercent.toFixed(2).replace('.', ',') + "%"
                                                    : "-"
                                            }
                                />

                                <TotalDeposit classes="bg-blue-500 mt-3"
                                              title={t('home.total_deposit')}
                                              icon={"/assets/img/svg/icons/white_chart_icon.svg"}
                                              badgeTitle={t('home.total_bonus')}
                                              linkTitle={"Invest History"}
                                              link={URLs.dashboard_invest}
                                              badge={userFullDetails ? currencyFormatter(userFullDetails.financial.totalBonusDeposit) : "-"}
                                              value={userFullDetails ? currencyFormatter(userFullDetails.financial.totalDeposit) : "-"}
                                />

                                <Articles/>
                            </div>
                        </div>
                        <div className="w-3/12" />
                </div>
                <div
                    className="col-span-5 bg-white pr-0 md:pr-3 h-full rounded-xl px-4 md:px-3 lg:px-4 div2 overflow-y-auto">
                    <div
                        className="w-full font-bold sticky top-0 bg-white pt-4">{t('home.top_performing_categories')}</div>

                    <div className="h-auto overflow-y-auto tiny-scroll-bar">
                        {/*<div className="grid grid-cols-3 border border-b-2 border-gray-200 pb-3 mt-2">*/}
                        {/*    <div className="text-sm text-center">Product Name</div>*/}
                        {/*    <div className="text-sm text-center">Sold Product</div>*/}
                        {/*    <div className="text-sm text-center">Total Sales</div>*/}
                        {/*</div>*/}
                        {
                            Props.getResourceDetailData && Props.getResourceDetailData.length === 0 && Props.getResourceDetailError
                                ? <div
                                    className="w-full h-full flex items-center justify-center text-center text-danger">{Props.getResourceDetailError}</div>
                                : Props.getResourceDetailLoading
                                ? <div className="w-full"><Loading/></div>
                                : Props.getResourceDetailData && Props.getResourceDetailData.length !== 0
                                    ?
                                    Props.getResourceDetailData.map((item, index) =>
                                        <GridRow
                                            key={index}>
                                            <div className="text-center ml-3">
                                                <div
                                                    className="font-bold index text-center text-gray-600 flex items-center justify-center">
                                                    {index + 1}.
                                                </div>
                                            </div>
                                            <div className=" mx-auto font-bold ">
                                                <div className="flex items-center justify-start ">
                                                    <div className="w-8 h-8 bg-gray-100 flex items-center justify-center rounded-full">
                                                        <img src={item.icon1} alt=""/>
                                                    </div>
                                                    <div className="w-28 ml-2">
                                                        <p>{item.title}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className=" mx-auto font-bold ">
                                                <div className="w-24">
                                                    <p className="rtl text-left">{currencyFormatter(parseFloat(item.amount))}</p>
                                                </div>
                                            </div>
                                            <div className=" mx-auto font-bold ">
                                                <div className="w-16">
                                                    <p>{item.percent ? readableNumber(parseFloat(parseFloat(item.percent).toFixed(2))) + "%" : "%"}</p>
                                                </div>
                                            </div>
                                        </GridRow>)

                                    : <div className=" mt-28 h-28 flex items-center justify-center">
                                        {
                                            localStorage.getItem('i18nextLng')?.includes('en')
                                                ? <img src="/assets/img/svg/bar-chart-nodata-en.svg" alt=""/>
                                                : <img src="/assets/img/svg/bar-chart-nodata-de.svg" alt=""/>
                                        }
                                    </div>
                        }
                    </div>
                </div>

                <div className="col-span-3 ml-0 md:ml-2 bg-white h-full rounded-xl px-0 pie-chart-container div3">
                    <div
                        className="w-full font-bold text-center text-sm mb-1">{t('home.profit_distribution_chart')}</div>

                    <div
                        className="pie-chart-container--tab mx-auto py-1 px-2 rounded-lg bg-link-water flex justify-between items-center font-bold text-center text-sm mb-0 xl:mb-2">
                            <span
                                className={` w-6/12 rounded-lg h-full inline-flex items-center justify-center text-center cursor-pointer
                                ${resourceDetail === "percent" ? "bg-white text-dodglerBlue" : "text-blueBell"}`}
                                onClick={() => setResourceDetail("percent")}>%</span>
                        <span
                            className={` w-6/12 rounded-lg h-full inline-flex items-center justify-center text-center cursor-pointer
                                        ${resourceDetail === "currency" ? "bg-white text-dodglerBlue" : "text-blueBell"}`}
                            onClick={() => setResourceDetail("currency")}>€</span>
                    </div>

                    <div className="mx-auto h-full">
                        {
                            Props.getResourceDetailError
                                ? <div
                                    className="text-danger text-center h-full flex items-center justify-center">{Props.getResourceDetailError}</div>
                                : Props.getResourceDetailData.length === 0 && Props.getResourceDetailLoading
                                ? <Loading/>
                                :
                                Props.getResourceDetailData.length === 0
                                    ? <div className=" mt-20 h-28 flex items-center justify-center">
                                        {
                                            localStorage.getItem('i18nextLng')?.includes('en')
                                                ? <img src="/assets/img/svg/pie-chart-nodata-en.svg" alt=""/>
                                                : <img src="/assets/img/svg/pie-chart-nodata-de.svg" alt=""/>
                                        }
                                    </div>
                                    : <DonutChart
                                        kind={resourceDetail}
                                        data={Props.getResourceDetailData.map(item => parseFloat(parseFloat(item.percent).toFixed(2)))}
                                        amount={Props.getResourceDetailData.map(item => parseFloat(parseFloat(item.amount).toFixed(2)))}
                                        colors={Props.getResourceDetailData.map(item => "#" + item.color)}
                                        labels={Props.getResourceDetailData.map(item => item.title)}
                                    />
                        }
                    </div>
                </div>
            </div>

                <div className="w-full mt-4 rounded-xl home--profit-second">
                    {
                        profitDataFilter.type==="Daily"
                        &&
                        <Tab
                            setProfitDataFilter={setProfitDataFilter} profitDataFilter={profitDataFilter}
                            title={t('home.daily_profit')}
                            data={Props.getResourceDetailWithDateDaily}
                            error={Props.getResourceDetailWithDateError}
                            loading={Props.getResourceDetailWithDateLoading}
                        />
                    }

                    {
                        profitDataFilter.type==="Weekly"
                        &&
                        <Tab
                            setProfitDataFilter={setProfitDataFilter} profitDataFilter={profitDataFilter}
                            title={t('home.weekly_profit')}
                            data={Props.getResourceDetailWithDateWeekly}
                            error={Props.getResourceDetailWithDateError}
                            loading={Props.getResourceDetailWithDateLoading}
                        />
                    }

                    {
                        profitDataFilter.type==="Monthly"
                        &&
                        <Tab
                            setProfitDataFilter={setProfitDataFilter} profitDataFilter={profitDataFilter}
                            title={t('home.monthly_profit')}
                            data={Props.getResourceDetailWithDateMonthly}
                            error={Props.getResourceDetailWithDateError}
                            loading={Props.getResourceDetailWithDateLoading}
                        />
                    }

                    {
                        profitDataFilter.type==="Yearly"
                        &&
                        <Tab
                            setProfitDataFilter={setProfitDataFilter} profitDataFilter={profitDataFilter}
                            title={t('home.yearly_profit')}
                            data={Props.getResourceDetailWithDateYearly}
                            error={Props.getResourceDetailWithDateError}
                            loading={Props.getResourceDetailWithDateLoading}
                        />
                    }

                    {Props.getProfitError
                        ? <div className="text-center text-danger w-full my-6 md:my-0">{Props.getProfitError}</div>
                        : Props.getProfitLoading
                            ? <Loading/>
                            : Props.getProfitData &&
                            Props.getProfitData.length !== 0
                                ?
                                <>
                                    <ProfitChart
                                        data={Props.getProfitData}
                                        paddingRight={Props.getProfitData}
                                        profitKind={profitDataFilter.type}
                                        bullet={false}
                                    />

                                    <ProfitBarChart
                                        data={Props.getProfitData}
                                        paddingRight={Props.getProfitData}
                                    />
                                </>
                                : <div className="mb-4 mt-4 live-arbitrage--graph ">
                                    <div className="live-arbitrage--table rounded-lg bg-white">
                                        <div className="header px-6 rounded-tl-xl">
                                            <span className="text-darkBlue font-bold">
                                              {profitDataFilter.type === "Daily" && t('home.daily_profit')}
                                                {profitDataFilter.type === "Weekly" && t('home.weekly_profit')}
                                                {profitDataFilter.type === "Monthly" && t('home.monthly_profit')}
                                                {profitDataFilter.type === "Yearly" && t('home.yearly_profit')}
                                                {t('home.chart')}
                                            </span>
                                        </div>
                                        <div className="flex justify-around items-center w-full h-64 my-20">
                                            {
                                                localStorage.getItem('i18nextLng')?.includes('en')
                                                    ? <img src="/assets/img/svg/line-chart-nodata-en.svg" alt=""/>
                                                    : <img src="/assets/img/svg/line-chart-nodata-de.svg" alt=""/>
                                            }
                                        </div>
                                    </div>
                                </div>
                    }
                </div>

        </div>
    );
};

const mapStateToProps = (state: any) => {
    return {
        getDailyProfitData: state.report.dailyProfitData.data,

        getProfitData: state.report.profitData.data,
        getProfitLoading: state.report.profitData.loading,
        getProfitError: state.report.profitData.error,

        getResourceDetailWithDateData: state.report.resourceDetailWithDate.data,
        getResourceDetailWithDateDaily: state.report.resourceDetailWithDate.daily,
        getResourceDetailWithDateWeekly: state.report.resourceDetailWithDate.weekly,
        getResourceDetailWithDateMonthly: state.report.resourceDetailWithDate.monthly,
        getResourceDetailWithDateYearly: state.report.resourceDetailWithDate.yearly,
        getResourceDetailWithDateLoading: state.report.resourceDetailWithDate.loading,
        getResourceDetailWithDateError: state.report.resourceDetailWithDate.error,

        getResourceDetailData: state.report.resourceDetail.data,
        getResourceDetailLoading: state.report.resourceDetail.loading,
        getResourceDetailError: state.report.resourceDetail.error,

        getMarketDetailLoading: state.report.marketDetail.loading,
        getMarketDetailError: state.report.marketDetail.error,

        getUserDetailsLoading: state.report.userFullDetails.loading,
        getUserDetailsError: state.report.userFullDetails.error,

        articles: state.articles.items,
    }
};


const mapDispatchToProps = (dispatch: any) => {
    return {
        GetUserFullDetails: () => dispatch(GetUserFullDetails()),
        GetMarketDetail: () => dispatch(GetMarketDetail()),
        GetResourceDetail: () => dispatch(GetResourceDetail()),
        GetResourceDetailWithDate: (data: any) => dispatch(GetResourceDetailWithDate(data)),
        GetProfitData: (data: any) => dispatch(GetProfitData(data)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);