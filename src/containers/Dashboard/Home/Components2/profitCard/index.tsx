import React from 'react';
import './index.scss';
import Chart from "react-apexcharts";

interface IProps {
    classes?: string;
    title: string;
    value: string;
    roi: string;
    chart: any[]
}

export default ({classes, title, value, chart, roi}: IProps) => {

    chart.length > 30 && chart.splice(0, chart.length - 30)
    const options = {
        series: [{
            data: chart
        }],
        chart: {
            type: 'line',
            width: 100,
            height: 35,
            paddingTop: 5,
            sparkline: {
                enabled: true
            },
        },
        labels: ['Total Profit'],
        tooltip: {
            fixed: {
                enabled: false
            },
            x: {
                show: false
                // title: {
                //   formatter: function () {
                //     return 'Total Profit'
                //   }
                // }
            },
            y: {
                title: {
                    formatter: function () {
                        return 'Amount(€):'
                    }
                }
            },
            marker: {
                show: false
            }
        },
        colors: ['#FFFFFF'],
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            width: 2,
            dashArray: 0,
        },
    };

    return (
        <div className={`card rounded-xl p-4 ${classes}`}>
            <div className="flex justify-between items-center">
                <div className="text-white">
                    <div>{title}</div>
                    <div className="text-xl font-semibold rtl">{value}</div>
                </div>
                <div>

                    <Chart
                        options={options}
                        series={options.series}
                        type="line"
                        width="100"
                        height="35"

                    />
                </div>
            </div>
            <div className="w-full p-1 bg-green-300 rounded-lg mt-2 text-center text-white">Roi {roi}</div>
        </div>
    );
};
