import React from 'react';
import './index.scss';

interface IProps {
	classes?: string;
	title: string;
	value: string
}

export default ({ classes , title, value}: IProps) => {
	return (
		<div className={`daily-profit-card flex justify-between items-center rounded-xl p-2 ${classes}`}>
			<span className="ml-2 text-thin text-sm text-black">{title}</span>
			<div className="flex justify-start items-center bg-green-200 p-1 rounded-lg daily-profit-card-total">
				<span className="ml-2 text-thin text-sm text-green-500 mr-2 rtl">{value}</span>
				<img src="/assets/img/svg/icons/small_chart.svg" className="w-0.5" />
			</div>
		</div>
	);
};
