import React, {useState} from 'react';
import {ValuesI} from "../interfaces";
import {ErrorMessage, Field, Form, Formik, FormikHelpers} from "formik";
import {stringDate2} from "../../../../assets/helpers/utilities";
import URLs from "../../../../constants/URLs";
// import fileIcon from "../../../../assets/img/svg/icons/file-icon-gray.svg";
import Loading from "../../../../components/UiKits/Loading/Loading";
import UserModel from "../../../../models/user.model";
import {KYCRequestI} from "../../../../reducers/kyc";
import {useTranslation} from "react-i18next";

const Countries = React.lazy(() => import("../../../../components/Countries/Countries"));
const FileUpload = React.lazy(() => import("../../../../components/UploadFile/UploadFile"));

interface IProps {
    userData: UserModel,
    kyc_request: KYCRequestI | null,
    onSubmit: any,
    countries: number,
    loading: boolean,
    error: string,
    status: string
}

const KYCForm = (Props: IProps) => {
    const [warning, setWarning] = useState<string | null>(null);
    const [documents, setDocuments] = useState(null);
    const [documentBackSides, setDocumentBackSides] = useState(null);
    const [bankStatements, setBankStatements] = useState(null);
    const [additionals, setAdditionals] = useState(null);
    const {
        t,
    } = useTranslation();
    const handleSelectFiles = (fileItems: any, name: string) => {
        if (name === 'documents') {
            setDocuments(fileItems.map((fileItem: any) => fileItem.file))
        }
        if (name === "documentBackSides") {
            setDocumentBackSides(fileItems.map((fileItem: any) => fileItem.file))
        }
        if (name === "bankStatements") {
            setBankStatements(fileItems.map((fileItem: any) => fileItem.file))
        }

        if (name === "additionals") {
            setAdditionals(fileItems.map((fileItem: any) => fileItem.file))
        }
    };

    return (
        <>
            <Formik
                initialValues={{
                    email: Props.userData.mail || "",
                    firstName: Props.userData.firstName || "",
                    lastName: Props.userData.lastName || "",
                    phone: Props.userData.phone ? "+" + Props.userData.phone.countryCode + " " + Props.userData.phone.number : "",
                    address: Props.kyc_request && Props.kyc_request.status === 'InCompleted' ? Props.kyc_request.address : "",
                    birthDate: Props.kyc_request && Props.kyc_request.status === 'InCompleted' ? Props.kyc_request.birthDate.substring(0, 10) : "",
                    country: Props.kyc_request && Props.kyc_request.status === 'InCompleted' ? Props.kyc_request.country.id : 0,
                }}
                // validate={}
                onSubmit={(
                    values: ValuesI,
                    {setSubmitting, setStatus, resetForm}: FormikHelpers<ValuesI>
                ) => {
                    if (!documents || !bankStatements || !documentBackSides) {
                        setWarning(t('kyc.please_upload_your_documents'))
                    } else {
                        values.country = parseFloat(String(values.country));
                        Props.onSubmit(values, {
                            documents,
                            additionals,
                            bankStatements,
                            documentBackSides
                        });
                    }
                }}
            >
                {({isSubmitting, status, values, setFieldValue}) => (
                    <Form className="edit-profile edit-profile--third-container pb-12">
                        <div className="w-full m-0 m-auto mt-2 flex flex-wrap gap-16 justify-start">
                            <div className="w-full">
                                <div className="relative">
                                    <Field as="select"
                                           readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                           required={true}
                                           className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                           id="country"
                                           name="country">
                                        {
                                            Props.kyc_request && Props.kyc_request.status === 'Pending'
                                                ? <option
                                                    value={Props.kyc_request.country.id}>{Props.kyc_request.country.name}</option>
                                                : <>
                                                    {
                                                        Props.kyc_request && Props.kyc_request.status === 'InCompleted'
                                                            ? <option
                                                                value={Props.kyc_request?.country.id}>{Props.kyc_request?.country.name}</option>
                                                            : <option
                                                                value="">{Props.countries === 0 ? `${t('confirmSection.loading')}...` : t('signUp.select_country')}</option>
                                                    }

                                                    <Countries kind="name" default={undefined}/>
                                                </>
                                        }

                                    </Field>
                                    <div className="pointer-events-none
                  absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                                        <svg className="fill-current h-6 w-6"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path
                                                d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                        </svg>
                                    </div>
                                </div>

                                <ErrorMessage name="country"/>
                            </div>
                        </div>
                        {
                            values.country || (Props.kyc_request && Props.kyc_request.status === 'Pending') || (Props.kyc_request && Props.kyc_request.status === 'InCompleted')
                                ?
                                <>
                                    <div
                                        className="w-full m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start fullname">
                                        <div className="flex-1">
                                            <label htmlFor="firstName">{t('kyc.firstName')}</label>
                                            <Field required={true} type="text" name="firstName"
                                                   readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                                   id="firstName"
                                                   value={Props.kyc_request && Props.kyc_request.status === 'Pending' ? Props.kyc_request.firstName : values.firstName}
                                                   placeholder={t('signUp.enter_your_name')}/>
                                            <ErrorMessage name="firstName"/>
                                        </div>
                                        <div className="flex-1">
                                            <label htmlFor="lastName">{t('kyc.lastName')}</label>
                                            <Field required={true} type="text" name="lastName"
                                                   id="lastName"
                                                   readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                                   value={Props.kyc_request && Props.kyc_request.status === 'Pending' ? Props.kyc_request.lastName : values.lastName}
                                                   placeholder={t('signUp.enter_your_last_name')}/>
                                            <ErrorMessage name="lastName"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
                                        <div className="w-full">
                                            <label htmlFor="email">{t('kyc.email_address')}</label>
                                            <Field required={true} type="email" name="email"
                                                   readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                                   id="email"
                                                   value={Props.kyc_request && Props.kyc_request.status === 'Pending' ? Props.kyc_request.email : values.email}
                                                   placeholder={t('kyc.enter_your_email_address')}/>
                                            <ErrorMessage name="email"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
                                        <div className="w-full">
                                            <label htmlFor="phone">{t('signUp.phone_number')}</label>
                                            <Field required={true} type="tel" name="phone"
                                                   readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                                   id="phone"
                                                   value={Props.kyc_request && Props.kyc_request.status === 'Pending' ? Props.kyc_request.phone : values.phone}
                                                   placeholder={t('signUp.enter_phone_number')}/>
                                            <ErrorMessage name="phone"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
                                        <div className="w-full">
                                            <label htmlFor="address">{t('kyc.full_address')}</label>
                                            <Field required={true} type="text" name="address"
                                                   readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                                   value={Props.kyc_request && Props.kyc_request.status === 'Pending' ? Props.kyc_request.address : values.address}
                                                   id="address"
                                                   placeholder={t('kyc.enter_your_full_address')}/>
                                            <ErrorMessage name="address"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
                                        <div className="w-full">
                                            <label htmlFor="birthDate">{t('kyc.birth_date')}</label>
                                            <Field required={true} type="date"
                                                   readOnly={Props.kyc_request && Props.kyc_request.status === 'Pending'}
                                                   value={Props.kyc_request && Props.kyc_request.status === 'Pending' ? stringDate2(Props.kyc_request.birthDate) : values.birthDate}
                                                   className="text-black" name="birthDate"
                                                   id="birthDate" placeholder="mm/dd/yyyy"/>
                                            <ErrorMessage name="birthDate"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-10 flex flex-wrap gap-16 justify-start file-input">
                                        <div className="w-full text-minsk">
                                            {
                                                Props.kyc_request && Props.kyc_request.status === 'Pending'
                                                    ? Props.kyc_request.attachments.map((item, index) => {
                                                        return (
                                                            <>
                                                                {
                                                                    item.type === 'Document' &&
                                                                    <div key={index}
                                                                         className="docPreview inline-block mx-3">
                                                                      <a href={URLs.base_URL + "/" + item.path}
                                                                         target="_blank"
                                                                         rel="noopener noreferrer">
                                                                        <img
                                                                          className="img mx-auto h-full"
                                                                          src={item.path.includes('pdf') ? "/assets/img/svg/icons/file-icon-gray.svg" : URLs.base_URL + "/" + item.path}
                                                                          alt=""/>

                                                                          {item.path.includes('pdf') &&
                                                                          <div
                                                                            className="button my-2">
                                                                            <span
                                                                              className="inline-block ">{item.name}</span>
                                                                          </div>}

                                                                      </a>
                                                                    </div>
                                                                }
                                                            </>
                                                        )
                                                    })
                                                    : <FileUpload
                                                        maxFiles={1}
                                                        setFiles={(e: any) => handleSelectFiles(e, "documents")}
                                                        files={documents} name={"documents"}/>
                                            }


                                            <p className="mt-5">
                                                {t('kyc.document_description')}
                                            </p>
                                            <ErrorMessage name="documents"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-10 flex flex-wrap gap-16 justify-start file-input">
                                        <div className="w-full">
                                            {
                                                Props.kyc_request && Props.kyc_request.status === 'Pending'
                                                    ? Props.kyc_request.attachments.map((item, index) => {
                                                        return (
                                                            <>
                                                                {
                                                                    item.type === 'DocumentBackSide' &&
                                                                    <div key={index}
                                                                         className="docPreview inline-block mx-3">
                                                                      <a href={URLs.base_URL + "/" + item.path}
                                                                         target="_blank"
                                                                         rel="noopener noreferrer">
                                                                        <img
                                                                          className="img mx-auto h-full"
                                                                          src={item.path.includes('pdf') ? "/assets/img/svg/icons/file-icon-gray.svg" : URLs.base_URL + "/" + item.path}
                                                                          alt=""/>

                                                                          {item.path.includes('pdf') &&
                                                                          <div
                                                                            className="button my-2">
                                                                            <span
                                                                              className="inline-block ">{item.name}</span>
                                                                          </div>}

                                                                      </a>
                                                                    </div>
                                                                }
                                                            </>
                                                        )
                                                    })
                                                    : <FileUpload
                                                        maxFiles={1}
                                                        setFiles={(e: any) => handleSelectFiles(e, "documentBackSides")}
                                                        files={documentBackSides}
                                                        name={"documentBackSides"}/>
                                            }

                                            <p className="mt-5">
                                                {t('kyc.licenses_description')}
                                            </p>
                                            <ErrorMessage name="documentBackSides"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-10 flex flex-wrap gap-16 justify-start file-input">
                                        <div className="w-full">
                                            {
                                                Props.kyc_request && Props.kyc_request.status === 'Pending'
                                                    ? Props.kyc_request.attachments.map((item, index) => {
                                                        return (
                                                            <>
                                                                {
                                                                    item.type === 'BankStatement' &&
                                                                    <div key={index}
                                                                         className="docPreview inline-block mx-3">
                                                                      <a href={URLs.base_URL + "/" + item.path}
                                                                         target="_blank"
                                                                         rel="noopener noreferrer">
                                                                        <img
                                                                          className="img mx-auto h-full"
                                                                          src={item.path.includes('pdf') ? "/assets/img/svg/icons/file-icon-gray.svg" : URLs.base_URL + "/" + item.path}
                                                                          alt=""/>

                                                                          {item.path.includes('pdf') &&
                                                                          <div
                                                                            className="button my-2">
                                                                            <span
                                                                              className="inline-block ">{item.name}</span>
                                                                          </div>}

                                                                      </a>
                                                                    </div>
                                                                }
                                                            </>
                                                        )
                                                    })
                                                    :
                                                    <FileUpload
                                                        maxFiles={1}
                                                        setFiles={(e: any) => handleSelectFiles(e, "bankStatements")}
                                                        files={bankStatements}
                                                        name={"bankStatements"}/>
                                            }

                                            <p className="mt-5">
                                                {t('kyc.bankStatements_description')}
                                            </p>
                                            <ErrorMessage name="bankStatements"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-10 flex flex-wrap gap-16 justify-start file-input">
                                        <div className="w-full">
                                            {
                                                Props.kyc_request && Props.kyc_request.status === 'Pending'
                                                    ? Props.kyc_request.attachments.map((item, index) => {
                                                        return (
                                                            <>
                                                                {
                                                                    item.type === 'Additional' &&
                                                                    <div key={index}
                                                                         className="docPreview inline-block mx-3">
                                                                      <a href={URLs.base_URL + "/" + item.path}
                                                                         target="_blank"
                                                                         rel="noopener noreferrer">
                                                                        <img
                                                                          className="img mx-auto h-full"
                                                                          src={item.path.includes('pdf') ? "/assets/img/svg/icons/file-icon-gray.svg" : URLs.base_URL + "/" + item.path}
                                                                          alt=""/>

                                                                          {item.path.includes('pdf') &&
                                                                          <div
                                                                            className="button my-2">
                                                                            <span
                                                                              className="inline-block ">{item.name}</span>
                                                                          </div>}

                                                                      </a>
                                                                    </div>
                                                                }
                                                            </>
                                                        )
                                                    })
                                                    :
                                                    <FileUpload
                                                        maxFiles={1}
                                                        setFiles={(e: any) => handleSelectFiles(e, "additionals")}
                                                        files={additionals} name={"additionals"} optional={true}/>
                                            }


                                            <p className="mt-5">
                                                {t('kyc.additional_description')}
                                            </p>
                                            <ErrorMessage name="additionals"/>
                                        </div>
                                    </div>

                                    <div
                                        className="w-full m-0 m-auto mt-12 flex flex-wrap gap-16 justify-start">
                                        <button type="submit"
                                                disabled={(Props.kyc_request && Props.kyc_request.status === 'Pending') || Props.loading || Props.status === "Waiting"}
                                                className="dodgler-button w-48 text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                                            {t('accounts_details.save')}
                                            {Props.loading && <Loading stylesProp2={{
                                                width: '35px',
                                                height: 'auto',
                                                position: 'absolute',
                                                top: "20%",
                                                left: '48%'
                                            }}/>}
                                        </button>
                                    </div>

                                    {
                                        warning
                                        &&
                                        <div role="alert" className="mt-4">
                                          <div
                                            className="border border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                            <span>{warning}</span>
                                          </div>
                                        </div>
                                    }
                                    {
                                        Props.error !== ""
                                        &&
                                        <div role="alert" className="mt-4">
                                          <div
                                            className="border border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                            <span>{Props.error}</span>
                                          </div>
                                        </div>
                                    }
                                    {
                                        Props.status === "Waiting"
                                        &&
                                        <div role="alert" className="mt-4">
                                          <div
                                            className="border border-green-400 rounded-b bg-green-100 px-4 py-3 text-green-700">
                                            <span>{t('kyc.waiting_for_upload')}</span>
                                          </div>
                                        </div>
                                    }
                                    {
                                        Props.status === "Success"
                                        &&
                                        <div role="alert" className="mt-4">
                                          <div
                                            className="border border-green-400 rounded-b bg-green-100 px-4 py-3 text-green-700">
                                            <span>{t('kyc.success_message')}</span>
                                          </div>
                                        </div>
                                    }
                                </>
                                : null
                        }
                    </Form>
                )}
            </Formik>
        </>
    );
}

export default KYCForm;
