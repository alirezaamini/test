import React, {useEffect} from 'react';
import './settings.scss'
import {useTranslation} from "react-i18next";
import EmailFrom from './components/form'
import {connect} from "react-redux";
import UserModel from "../../../models/user.model";
import {UpdateSettings} from "../../../actions/auth";
import {POST_EMAIL_SETTINGS_FAILURE} from "../../../actions/auth/AuthActionTypes";

interface IProps {
  user: UserModel,
  update:any,
  clearStore:any,
  emailLoading: boolean,
  emailStatus: string,
  emailError: string
}

const Setting = (Props: IProps) => {

  useEffect(()=>{
    return () => {
      Props.clearStore()
    }
  },[])

  const {
    t,
  } = useTranslation();

  return (
    <div className="settings mt-8">
      <div className="bg-white w-full mx-auto my-10 sm:px-6 md:px-10 pt-10 pb-16 rounded-xl ">
        <div className="flex justify-between items-center">
          <div>
            <h6 className="text-xl font-weight-600 text-minsk">
              {t('settings.email_settings')}
            </h6>
          </div>

          {/*<div>*/}
          {/*  <button*/}
          {/*    onClick={()=>setSubmitAll(true)}*/}
          {/*          className="withdrawal-button w-48 text-white font-bold py-3 px-4 rounded-xl focus:outline-none">*/}
          {/*    {t('accounts_details.save')}*/}
          {/*  </button>*/}
          {/*</div>*/}
        </div>


        <EmailFrom
          onSubmit={(data:any) => Props.update(data)}
          user={Props.user}
          loading={Props.emailLoading}
          error={Props.emailError}
          status={Props.emailStatus}
        />
      </div>
    </div>
  );
}


const mapStateToProps = (state: any) => {
  return {
    user: state.auth.user,

    emailLoading: state.auth.setting.email.loading,
    emailStatus: state.auth.setting.email.status,
    emailError: state.auth.setting.email.error,

  }
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    update: (data:any) => dispatch(UpdateSettings(data)),
    clearStore: () => dispatch({
      type: POST_EMAIL_SETTINGS_FAILURE,
      payload: {
        error: ""
      }
    })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Setting);
