import React, {useEffect, useState} from 'react';
import Loading from "../../../components/UiKits/Loading/Loading";
import './live-arbitrage.scss';
import {connect} from 'react-redux';
import {
    ArbitrageGraphFilter, ArbitrageGraphFilterDefaultState,
    FetchLiveArbitrage, FetchLiveArbitrageGraph, FetchLiveSMBGraph,
    ICurrencies,
    IExchangers,
    setLiveArbitrage
} from "../../../actions/arbitrage";
import {LiveArbitrageDefaultStateI} from "../../../reducers/arbitrage";
import {useTranslation} from "react-i18next";
import ArbitrageTable from "./components/ArbitrageTable";
import SMBGraph from "./components/SMBGraph";

const CustomChart = React.lazy(() => import("../../../components/Charts/Line/Chart"));
const CandleStickChart = React.lazy(() => import("../../../components/Charts/Line/CandleStickChart"));

interface IProps {
    fetchLiveArbitrage: any,
    fetchLiveArbitrageGraph: any,
    deleteLiveArbitrage: any,
    fetchLiveSMBGraph: any,
    currencies: ICurrencies[],
    exchangers: IExchangers[],
    // graph: CoinMarketArbitrageGraphResponse,
    linear: any,
    candle: any,
    lastUpdate: string,
    error: string,
    graphError: string,
    graphLoading: boolean,
    smb: any[],
    smbLoading: boolean,
    smbError: string,
}

const Index = (Props: IProps) => {
    const [chartType, setChartType] = useState<string>("linear")

    useEffect(() => {
        Props.fetchLiveArbitrage();
        Props.fetchLiveSMBGraph();
        let arbitrageInterval = setInterval(function () {
            Props.fetchLiveArbitrage();
            Props.fetchLiveSMBGraph();
        }, 15000);

        return function cleanup() {
            Props.deleteLiveArbitrage({currencies: [], exchangers: []});
            clearInterval(arbitrageInterval);
        };
    }, []);

    const [graphFilter, setGraphFilter] = useState<ArbitrageGraphFilter>(ArbitrageGraphFilterDefaultState);

    const handleFilterGraph = (key: string, value: string, key2: string, value2: string) => {
        setGraphFilter({...graphFilter, [key]: value, [key2]: value2})
    };

    useEffect(() => {
        Props.fetchLiveArbitrageGraph(graphFilter);
    }, [graphFilter]);

    const {t} = useTranslation();
    return (
        <div className="live-arbitrage mt-10">

            <ArbitrageTable lastUpdate={Props.lastUpdate}
                            currencies={Props.currencies}
                            exchangers={Props.exchangers}
                            error={Props.error}/>

            <SMBGraph smbError={Props.smbError}
                      smbLoading={Props.smbLoading}
                      smb={Props.smb}
            />

            <div className="my-24 live-arbitrage--graph">
                <div className="flex flex-wrap justify-start items-center">
                    <div className="relative mr-3">
                        <select name="coin_type" id="coin_type"
                                className="text-black font-medium px-4 py-3  pr-8
                                                block appearance-none bg-white rounded
                                                leading-tight focus:outline-none"
                                onChange={(e: any) => handleFilterGraph('symbol', e.target.value, 'time_period', graphFilter.time_period)}
                        >
                            <option value="btc">BTC</option>
                            <option value="eth">ETH</option>
                            <option value="ltc">LTC</option>
                            <option value="bch">BCH</option>
                            <option value="bnb">BNB</option>
                            <option value="link">LINK</option>
                            <option value="dot">DOT</option>
                        </select>
                        <div
                            className="pointer-events-none font-bold absolute inset-y-0 right-0 flex items-center px-2 text-salmon">
                            <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 20 20">
                                <path
                                    d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </div>
                    </div>

                    <div className="relative mx-3">
                        <select name="line" id="line"
                                onChange={(e: any) => setChartType(e.target.value)}
                                className="text-black font-medium px-4 py-3  pr-8
                                  block appearance-none bg-white rounded
                                  leading-tight focus:outline-none">
                            <option value="linear">Line</option>
                            <option value="candle">Candle</option>
                        </select>
                        <div
                            className="pointer-events-none font-bold absolute inset-y-0 right-0 flex items-center px-2 text-salmon">
                            <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 20 20">
                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </div>
                    </div>
                </div>


                <div className="live-arbitrage--table mt-6 rounded-lg bg-white">
                    <div className="header py-6 px-6 rounded-tl-xl">
                        <span className="text-minsk font-bold">{t('live_arbitrage.live_arbitrage_graph')}</span>
                    </div>

                    <form>
                        <div
                            className="coin-filters my-3 w-full flex flex-wrap justify-between items-center py-6 px-6">
                            <div className="fiats my-3 md:my-0 flex-auto flex justify-start items-center">
                                <div className="fiats-item mx-2 ">
                                    <input
                                        onChange={(e) => handleFilterGraph('convert', e.target.id, 'time_period', graphFilter.time_period)}
                                        type="checkbox" name="currency"
                                        id="EUR" checked={graphFilter.convert === "EUR"}/>

                                    <label htmlFor="EUR" className=" px-6 py-2">
                                        EUR
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div
                            className="date-filters my-3 md:my-0 w-full flex flex-wrap justify-between items-center py-6 px-6">
                            <div className="dates flex-auto flex flex-wrap justify-start items-center">
                                <div className="dates-item m-1 md:m-2 ">
                                    <input
                                        onChange={(e) => handleFilterGraph('interval', e.target.name, 'time_period', 'daily')}
                                        type="checkbox" name="1d"
                                        id="oneDay" checked={graphFilter.interval === "1d"}/>

                                    <label htmlFor="oneDay" className="px-2 md:px-3 py-2">
                                        {t('live_arbitrage.daily')}
                                    </label>
                                </div>

                            </div>
                        </div>
                    </form>

                    <div className="w-full pr-10 custom-chart">
                        {
                            Props.graphError !== ""
                                ? <p className="text-danger text-center">{Props.graphError}</p>
                                : Props.graphLoading
                                ? <Loading/>
                                : <>
                                    {
                                        chartType === "linear"
                                            ?
                                        <CustomChart
                                                     data={Props.linear.quotes}
                                                     candleData={Props.candle.quotes}
                                                     paddingRight={Props.linear.quotes}
                                        />
                                        :<CandleStickChart
                                                     candleData={Props.candle.quotes}
                                                     paddingRight={Props.candle.quotes}
                                        />

                                    }

                                    <p className="absolute text-minsk">{t('live_arbitrage.volume')}</p>
                                </>
                        }


                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state: any) => {
    return {
        currencies: state.market.currencies,
        exchangers: state.market.exchangers,
        lastUpdate: state.market.lastUpdate,
        error: state.market.error,
        linear: state.market.graph.linear,
        candle: state.market.graph.candle,
        graphLoading: state.market.graph.loading,
        graphError: state.market.graph.error,
        smb: state.market.smb.data,
        smbLoading: state.market.smb.loading,
        smbError: state.market.smb.error,

    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchLiveArbitrage: () => dispatch(FetchLiveArbitrage()),
        fetchLiveArbitrageGraph: (graphFilter: ArbitrageGraphFilter) => dispatch(FetchLiveArbitrageGraph(graphFilter)),
        fetchLiveSMBGraph: () => dispatch(FetchLiveSMBGraph()),
        deleteLiveArbitrage: (data: LiveArbitrageDefaultStateI) => dispatch(setLiveArbitrage(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);