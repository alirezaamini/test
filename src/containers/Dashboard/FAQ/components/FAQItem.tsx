import React, {useState} from 'react';
import {FAQItemI} from "../../../../actions/faq";
import '../faq.scss'

const FAQItem = (Props: FAQItemI) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <div className={
      `faq-container--list-item ${isOpen && "active"}`
    }>
      <div onClick={() => setIsOpen(prevState => !prevState)}
           className="faq-header w-full flex flex-no-wrap justify-between h-20 items-center">
        <div className="title flex-auto">{Props.title}</div>
        <div className="icon flex-auto"/>
      </div>

      <div className="body w-full">
        <p>
          {Props.content}
        </p>
      </div>
    </div>
  );
};

export default FAQItem;
