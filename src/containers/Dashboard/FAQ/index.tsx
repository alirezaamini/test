import React, {Suspense, useEffect} from 'react';
import Loading from "../../../components/UiKits/Loading/Loading";
import './faq.scss';
import {connect} from 'react-redux';
import {FAQItemI, FetchFAQ} from "../../../actions/faq";
import {useTranslation} from "react-i18next";

const FAQItem = React.lazy(() => import("./components/FAQItem"));

interface IProps {
    items: FAQItemI[],
    loading: boolean,
    error: string,
    FetchFAQ: any
}

const Index = (Props: IProps) => {

    useEffect(() => {
        const lang = localStorage.getItem('i18nextLng')?.includes('en') ? 'en' : 'de'
        Props.FetchFAQ(lang);
    }, [localStorage.getItem('i18nextLng')]);
    const {
        t,
    } = useTranslation();

    return (
        <div className="faq mt-12 w-full bg-white pt-10 pb-20">
            <div className="pl-12 my-4 home-title text-minsk text-normal">{t('faq.header')}</div>

            <div className="faq-container pt-5 w-full bg-white border-xl">
                {
                    Props.error !== ""
                        ? <div className="w-full flex justify-center items-center">
                            <div role="alert" className="mt-4">
                                <div
                                    className="border border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    <span>{Props.error}</span>
                                </div>
                            </div>
                        </div>
                        : Props.loading
                        ? <div className="w-full flex justify-center items-center"><Loading/></div>
                        : <div className="faq-container--list">
                            {
                                Props.items.map((item, index) => <FAQItem key={index} {...item}/>)
                            }
                        </div>
                }
            </div>
        </div>
    );
};

const mapStateToProps = (state: any) => {
    return {
        items: state.faq.items,
        loading: state.faq.loading,
        error: state.faq.error,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        FetchFAQ: (lang: string) => dispatch(FetchFAQ(lang))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
