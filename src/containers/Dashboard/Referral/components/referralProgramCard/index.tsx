import React from 'react'
import {useTranslation} from "react-i18next";

interface IProps {
    reward: number | string,
    referrals:number | string,
}

export default (Props: IProps) => {

    const {
        t,
    } = useTranslation();

    return <div className="bg-white rounded-xl p-10 flex justify-between items-center relative ">
        <div className="flex flex-wrap lg:flex-no-wrap justify-start items-start">
            <div className="flex justify-start items-center">
                <img src="/assets/img/svg/icons/blue_gift.svg" alt=""/>
                <div className="font-semibold text-lg text-blue-900 lg:hidden ml-6">Referral Program</div>
            </div>
            <div className="ml:0 lg:ml-6">
                <div className="font-semibold text-lg text-blue-900 sm:hidden lg:block">Referral Program</div>
                <div className="font-normal text-base text-blue-900 my-4">{t('referral_program.title')}</div>
                <div className="flex justify-start items-center mt-10">
                    <div className="p-2 bg-blue-100 rounded-xl w-32 lg:w-auto mr-6 text-blue-500 border-1 border-gray-300 text-center">
                        <span className="mr-1">{t('referral_program.your_reward')}:</span><b>{Props.reward}</b>
                    </div>
                    <div className="p-2 bg-blue-100 rounded-xl w-32 lg:w-auto text-blue-500 border-1 border-gray-300 text-center">
                        <span className="mr-1">  {t('referral_program.referrals')}:</span><b>{Props.referrals}</b>
                    </div>
                </div>
            </div>
        </div>
        <img src="/assets/img/svg/icons/speaker.svg" alt="" className="absolute top-0 right-0"/>
    </div>
}