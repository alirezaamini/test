import CountryModel, {countryDefaultState} from "../../../models/country.model";

export interface errorsType {
  email?: string,
  firstname?: string,
  lastname?: string,
  phone?: string,
  currentPassword?: string,
  newPassword?: string,
  confirmNewPassword?: string,
}


export const initialErrors: errorsType = {
  email: undefined,
  firstname: undefined,
  lastname: undefined,
  phone: undefined,
  currentPassword: undefined,
  newPassword: undefined,
  confirmNewPassword: undefined,
}

export interface Values {
  email: string,
  firstname: string,
  lastname: string,
  phone: string,
  currentPassword: string,
  newPassword: string,
  confirmNewPassword: string,
  country: any,
  code: any,
}


export interface firstFormValues {
  additional_phone_number: string,
  avatar: any,
  gender: string
}

export const firstFormInitialValues: firstFormValues = {
  additional_phone_number: "",
  avatar: "",
  gender: ""
}

export interface secondFormValues {
  country_code: string,
  phone_number: string,
}

export const secondFormInitialValues: secondFormValues = {
  country_code: "",
  phone_number: "",
}

export interface bankFormValues {
  bitcoin: string,
  ethereum: string,
  eurNameOfTheClient: string,
  eurAddressOfTheClient: string,
  eurBankName: string,
  eurBankAddress: string,
  eurIBAN: string,
  eurSwiftOrBIC: string,
  eurCountryClient: CountryModel,
  eurCountryBank: CountryModel,
  eurComments: string,
}

export const bankFormInitialValues = {
  bitcoin: "",
  ethereum: "",
  eurNameOfTheClient: "",
  eurAddressOfTheClient: "",
  eurBankName: "",
  eurBankAddress: "",
  eurIBAN: "",
  eurSwiftOrBIC: "",
  eurCountryClient: countryDefaultState,
  eurCountryBank: countryDefaultState,
  eurComments: "",
}

export const initialValues: Values = {
  email: "",
  firstname: "",
  lastname: "",
  phone: "",
  currentPassword: "",
  newPassword: "",
  confirmNewPassword: "",
  country: "",
  code: "",
}

