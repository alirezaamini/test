import React, {Suspense, useEffect, useState} from 'react';
import {Link, Redirect} from "react-router-dom";
import URLs from "../../../constants/URLs";
import {initialValues, Values} from "./interfaces";
import {Form, Formik, Field, FormikHelpers} from "formik";
import {Decrypt} from "../../../apis/encryption/encryption/encryption";
import AuthVerifySubmitDtoModel from "../../../models/responses/authVerifySubmitDto.model";
import UserModel from "../../../models/user.model";
import {LoginAction} from "../../../actions/auth";
import {connect} from "react-redux";
import qs from 'qs';
import Loading from "../../../components/UiKits/Loading/Loading";
import {LevelItemInterface} from "../../../models/levels.model";
import {useTranslation} from "react-i18next";
import {PostVerifyService} from "../../../apis/auth";
import {toast} from "react-toastify";
import {getLocalStorage} from "../../../assets/helpers/storage";
import StepInput from "../../../components/UiKits/stepInput/index";

const Alert = React.lazy(() => import('../../../components/UiKits/Warning/Warning'));

const Index = (Props: any) => {
  const [isSubmited, setIsSubmited] = useState<boolean>(false);
  const [warning, setWarning] = useState<string>("");
  const [redirectToSignUp, setRedirectToSignUp] = useState<boolean>(false);
  const [email, setEmail] = useState<string>("");

  useEffect(() => {
    const query: any = qs.parse(Props.location.search, {ignoreQueryPrefix: true});
    if (Props.location.search) {
      localStorage.setItem('id', query.p)
    } else if (!localStorage.getItem('id')) {
      setRedirectToSignUp(true);
    }

    query.e && localStorage.setItem('email', query.e)
    localStorage.getItem('email') && setEmail(localStorage.getItem('email') || "")

    return function cleanup() {
      setRedirectToSignUp(false);
      setWarning("")
      localStorage.removeItem("email")
    };
  }, [Props.location]);

  const {
    t,
  } = useTranslation();

  return (
    <Suspense fallback={<Loading/>}>

      <Formik
        initialValues={initialValues}
        onSubmit={async (
          values: Values,
          {setSubmitting}: FormikHelpers<Values>
        ) => {
          try {
            setIsSubmited(true);

            let data: AuthVerifySubmitDtoModel = {
              id: localStorage.getItem('id') || "",
              code: values.code,
            };
            console.log(data);

            // code : "987654"
            // code: values.code
            const res = await PostVerifyService(data);
            if (res.status === 201 || res.status === 200) {
              toast.dismiss();
              //  redirect to dashboard home page
              localStorage.setItem('IP', JSON.parse(Decrypt(res.data.data)).isPremium);
              localStorage.setItem('HI', JSON.parse(Decrypt(res.data.data)).hasInvest);
              localStorage.setItem('CI', JSON.parse(Decrypt(res.data.data)).canInvest);
              localStorage.setItem('userAuth', JSON.parse(Decrypt(res.data.data)).token);
              localStorage.setItem('userData', JSON.stringify(JSON.parse(Decrypt(res.data.data)).user));
              localStorage.setItem('userLevel', JSON.stringify(JSON.parse(Decrypt(res.data.data)).level));
              localStorage.setItem('memberships', JSON.stringify(JSON.parse(Decrypt(res.data.data)).memberships));

              let canWithdrawal = JSON.parse(Decrypt(res.data.data)).canWithdrawal || false;
              let canWithdrawalAt = JSON.parse(Decrypt(res.data.data)).canWithdrawalAt || false;

              localStorage.setItem('canWithdrawal', JSON.stringify(canWithdrawal));
              localStorage.setItem('canWithdrawalAt', JSON.stringify(canWithdrawalAt));

              localStorage.removeItem('id');
              Props.LoginAction({
                user: JSON.parse(Decrypt(res.data.data)).user,
                level: JSON.parse(Decrypt(res.data.data)).level,
                hasInvest: JSON.parse(Decrypt(res.data.data)).hasInvest,
                canInvest: JSON.parse(Decrypt(res.data.data)).canInvest,
                isPremium: JSON.parse(Decrypt(res.data.data)).isPremium,
              });
              let registerMembership = getLocalStorage('userData').registerMembership || JSON.parse(Decrypt(res.data.data)).memberships[0].slug

              if(registerMembership){
                if(registerMembership==="arbitrage-investor"){
                  window.location.href = URLs.dashboard_home
                }
                if(registerMembership==="token-investor"){
                  window.location.href = URLs.token_dashboard_home
                }
              }else {
                window.location.href = URLs.dashboard_home
              }

            } else {
              // setWarning(t('confirmSection.try_again'))
            }
            setSubmitting(false);
            setIsSubmited(false);

          } catch (err) {
            // if (err && err.status) {
            //   setWarning(JSON.parse(Decrypt(err.data.data)).client_message)
            // } else {
            //   setWarning(t('confirmSection.try_again'))
            // }
            setSubmitting(false);
            setIsSubmited(false);
          }
        }}
      >
        {({values, setFieldValue}) => (
          <Form className="px-8 pt-6 pb-8 mt-4 mb-4">
            {redirectToSignUp && <Redirect to={{pathname: URLs.register}}/>}
            <div className="mb-6 text-left headTitle">
              <div className="inline-block"><h6 className="font-bold text-black ">{t('verify.header')}</h6></div>
              <div>
                <p>
                  {t('verify.description', {email: email})}
                </p>
              </div>
            </div>
            <div className="w-full mt-12 mb-6">
                <div className="mb-6">
                  <StepInput count={6} spaceX={10} color='#38327c' onEnd={(val: string) => {
                    setFieldValue('code',val)
                  }} />
                </div>
            </div>

            <div className="mt-12 w-full flex items-center justify-center">
              <button
                disabled={isSubmited}
                className="login-container--button w-full text-white font-bold py-3 px-4 rounded focus:outline-none relative"
                type="submit">
                {t('verify.button')}
                {isSubmited &&
                <Loading stylesProp={{width: '35px', height: 'auto', position: 'absolute', top: "20%", left: '48%'}}/>}
              </button>
            </div>
            <div className="flex items-center justify-center mt-4">
              {
                warning !== ""
                &&
                <Alert warning={warning} setWarning={(data: string) => setWarning(data)}/>
              }
            </div>
            <div className="flex items-center justify-start flex-wrap mt-4">
              <Link to={URLs.login}
                    className="block align-baseline text-sm text-gray-500 hover:text-blue-800">
                {t('verify.remember_password')} &nbsp; <span
                className="text-dark-blue">{t('verify.back_to_login')}</span>
              </Link>
            </div>
          </Form>
        )}
      </Formik>
    </Suspense>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    LoginAction: (data: { user: UserModel, level: LevelItemInterface, hasInvest: boolean, canInvest: boolean, isPremium: boolean }) => dispatch(LoginAction(data))
  }
};

export default connect(null, mapDispatchToProps)(Index);
