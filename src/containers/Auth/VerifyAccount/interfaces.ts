export interface errorsType {
  code?: string,
}


export const initialErrors : errorsType = {
  code: undefined,
}

export interface Values {
  code?: string,
}


export const initialValues : Values = {
  code: "",
}