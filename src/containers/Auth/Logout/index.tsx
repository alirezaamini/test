import React, {useEffect} from 'react';
import URLs from "../../../constants/URLs";

const Index = () => {
  useEffect(() => {
    const lang: string | null = localStorage.getItem('i18nextLng');
    const modalMessage: any = localStorage.getItem('modalMessage');
    if (modalMessage) {
      localStorage.clear();
      lang ? localStorage.setItem('i18nextLng', lang) : localStorage.setItem('i18nextLng', 'de-GE')
      localStorage.setItem('modalMessage',modalMessage);
      window.location.href = `${URLs.app_site_URL}/auth/login`;
    } else {
      localStorage.clear();
      lang ? localStorage.setItem('i18nextLng', lang) : localStorage.setItem('i18nextLng', 'de-GE')
      window.location.href = "https://pntcap.com"
    }
  }, [])

  return (<></>)
}

export default Index;
