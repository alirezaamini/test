import {Values, errorsType} from "./interfaces";
import {strongRegex} from "../SignUp/validate";

export const validate = (values : Values) => {
  const errors : errorsType = {};

  if(!values.code){
    errors.code = "Required"
  }

  if(!values.password){
    errors.password = "Required"
  }else if(strongRegex(values.password) < 2) {
    errors.password = "Enter stronger password"
  }else if(values.password.length < 8) {
    errors.password = "Minimum length is 8"
  }

  if(!values.confirmPassword){
    errors.confirmPassword = "Required"
  }else if(values.password !== values.confirmPassword) {
    errors.confirmPassword = "Passwords Don't Match"
  }

  return errors;
};
