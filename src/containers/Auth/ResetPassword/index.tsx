import React, {Suspense, useEffect, useState} from 'react';
import {Link, Redirect} from "react-router-dom";
import URLs from "../../../constants/URLs";
import {initialValues, Values} from "./interfaces";
import {Formik, Form, Field, FormikHelpers, ErrorMessage} from "formik";
import {validate} from "./validate";
import Loading from "../../../components/UiKits/Loading/Loading";
import {useTranslation} from "react-i18next";
import {PostResetPasswordService} from "../../../apis/auth";
import {toast} from "react-toastify";

const Alert = React.lazy(() => import('../../../components/UiKits/Warning/Warning'));

const Index = () => {

    const [isSubmited, setIsSubmited] = useState<boolean>(false);
    const [warning, setWarning] = useState<string>("");
    const [redirect, setRedirect] = useState<boolean>(false);
    const [redirectToForget, setRedirectToForget] = useState<boolean>(false);
    const [hidePw, setHidePw] = useState<boolean>(true);
    const [hideConfirmPw, setHideConfirmPw] = useState<boolean>(true);

    useEffect(() => {
        if (!localStorage.getItem('userId')) {
            setRedirectToForget(true)
        }
    }, []);

    const {
        t,
    } = useTranslation();
    return (
        <Suspense fallback={<Loading/>}>

            <Formik
                initialValues={initialValues}
                validate={validate}
                onSubmit={async (
                    values: Values,
                    {setSubmitting}: FormikHelpers<Values>
                ) => {
                    try {
                        setIsSubmited(true);

                        //send apis
                        let data = {
                            id: localStorage.getItem('userId') || 0,
                            password: values.password,
                            code: values.code?.trim(),
                        };

                        const res = await PostResetPasswordService(data);

                        if (res.status === 201) {
                            toast.dismiss();
                            //  redirect to dashboard home page
                            localStorage.removeItem('userId');
                            setRedirect(true);
                        }
                        setSubmitting(false);
                        setIsSubmited(false);
                    } catch (err) {
                        if (err && err.status) {
                            setWarning(err.client_message)
                        } else {
                            setWarning(t('confirmSection.try_again'))
                        }
                        setSubmitting(false);
                        setIsSubmited(false);
                    }
                }}
            >
                {({values}) => (
                    <Form className="px-8 pt-6 pb-8 mt-4 mb-4">
                        {redirect && <Redirect to={{pathname: URLs.login}}/>}
                        {redirectToForget && <Redirect to={{pathname: URLs.forgotPassword}}/>}
                        <div className="mb-6 text-left headTitle">
                            <div className="inline-block"><h6
                                className="font-bold text-black ">{t('reset_password.header')}</h6></div>
                            <div><p>
                                {t('reset_password.description')}
                            </p></div>
                        </div>
                        <div className="mt-12 mb-6">
                            <label className="block login-container--label
               text-left text-gray-700 text-sm font-bold mb-4" htmlFor="code">
                                {t('reset_password.confirm_code')}
                            </label>
                            <div className="form--control hide" id="code">

                                <Field
                                    required={true}
                                    value={values.code}
                                    autoComplete="one-time-code"
                                    className=" login-container--input
                appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                    id="code" name="code" type="text" placeholder={t('verify.enter_code')}/>
                            </div>

                            <div className="field-error text-red-600 text-left">
                                <ErrorMessage name="code"/>
                            </div>
                        </div>
                        <div className="mt-8 mb-6">
                            <label className="block login-container--label
               text-left text-gray-700 text-sm font-bold mb-4" htmlFor="password">
                                {t('reset_password.new_password')}
                            </label>
                            <div className="form--control" id="passwordInput">

                                <Field
                                    value={values.password}
                                    minLength={8}
                                    required={true}
                                    className="appearance-none border login-container--input
                border-red-500 rounded w-full py-4 px-3 text-gray-700 mb-3 leading-tight focus:outline-none"
                                    id="password"
                                    name="password"
                                    type={hidePw ? "password" : "text"}
                                    placeholder={t('reset_password.enter_new_password')}/>
                                <span onClick={() => setHidePw((prevHidePw: boolean) => !prevHidePw)}
                                      className="flex justify-center align-middle cursor-pointer">
              {
                  hidePw
                      ? <img src={"/assets/img/svg/icons/hide-input-icon.svg"} alt=""/>
                      : <img src={"/assets/img/svg/icons/Eye.svg"} alt=""/>
              }
            </span>

                                <div className="field-error text-red-600 text-left">
                                    <ErrorMessage name="password"/>
                                </div>
                            </div>

                        </div>
                        <div className="mt-8 mb-6">
                            <label className="block login-container--label
               text-left text-gray-700 text-sm font-bold mb-4" htmlFor="confirmPassword">
                                {t('reset_password.confirm_new_password')}
                            </label>
                            <div className="form--control" id="passwordInput">

                                <Field
                                    value={values.confirmPassword}
                                    minLength={8}
                                    required={true}
                                    className="appearance-none border login-container--input
                border-red-500 rounded w-full py-4 px-3 text-gray-700 mb-3 leading-tight focus:outline-none"
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    type={hideConfirmPw ? "password" : "text"}
                                    placeholder={t('reset_password.confirm_your_new_password')}/>
                                <span
                                    onClick={() => setHideConfirmPw((prevHideConfirmPw: boolean) => !prevHideConfirmPw)}
                                    className="flex justify-center align-middle cursor-pointer">
                  {
                      hideConfirmPw
                          ? <img src={"/assets/img/svg/icons/hide-input-icon.svg"} alt=""/>
                          : <img src={"/assets/img/svg/icons/Eye.svg"} alt=""/>
                  }
                </span>

                            </div>

                            <div className="field-error text-red-600 text-left">
                                <ErrorMessage name="confirmPassword"/>
                            </div>

                        </div>
                        <div className="flex items-center justify-center">
                            <button
                                disabled={isSubmited}
                                className="login-container--button w-full text-white font-bold py-3 px-4 rounded focus:outline-none"
                                type="submit">
                                {t('reset_password.header')}
                                {isSubmited && <Loading stylesProp={{
                                    width: '35px',
                                    height: 'auto',
                                    position: 'absolute',
                                    top: "20%",
                                    left: '48%'
                                }}/>}
                            </button>
                        </div>
                        <div className="flex items-center justify-center mt-4">
                            {
                                warning !== ""
                                &&
                                <Alert warning={warning} setWarning={(data: string) => setWarning(data)}/>
                            }
                        </div>
                        <div className="flex items-center justify-start flex-wrap mt-4">
                            <Link to={URLs.login}
                                  className="block align-baseline text-sm text-gray-500 hover:text-blue-800">
                                {t('verify.remember_password')} &nbsp; <span
                                className="text-dark-blue">{t('verify.back_to_login')}</span>
                            </Link>
                        </div>
                    </Form>
                )}
            </Formik>
        </Suspense>
    );
};


export default Index;
