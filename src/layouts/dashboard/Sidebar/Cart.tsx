import React from 'react';
import './cart.scss';
import {Link} from 'react-router-dom';

interface IProps {
    icon: string,
    title: string,
    link: string,
    value: string
}

const SidebarCart = (Props: IProps) => {

    return (
        <div className="info-cart2 flex flex-wrap items-center justify-between mt-2 lg:mt-4 mx-2">
            <div className="w-auto flex flex-nowrap justify-start items-center h-full">
                <div className="image flex items-center justify-center">
                    <img src={Props.icon} alt=""/>
                </div>
                <div className="pl-5">
                    <div className="text text-darkBlue ">{Props.title}</div>
                    <div className="font-bold body"><p className="rtl text-left">{Props.value}</p></div>
                </div>
            </div>
            <Link to={Props.link}>
                <img src={"/assets/img/svg/icons/arrow-right-circle.svg"} alt=""/>
            </Link>
        </div>
    );
};

export default SidebarCart;

