import React, {Suspense, useState} from 'react';
import {connect} from 'react-redux';
import './sidebar.scss';
import Loading from "../../../components/UiKits/Loading/Loading";
import UserModel from "../../../models/user.model";
import DropDown from "../../../components/DropDowns/DropDown";
import {checkAvatar, currencyFormatter, handleAvatarError, readableNumber} from "../../../assets/helpers/utilities";
import URLs from "../../../constants/URLs";
import {useTranslation} from "react-i18next";
import {Link} from 'react-router-dom'
import InfoCart from "../../../layouts/dashboard/Sidebar/Cart";
import MarketModel from "../../../models/market.model";
import {getLocalStorage} from "../../../assets/helpers/storage";
import TotalDeposit from "../../../containers/Dashboard/Home/Components2/totalDeposit";
import UserFullDetailsModel from "../../../models/userFullDetails.model";
import moment from "moment-timezone";
import {ProfileDropDown} from "../Header/ProfileDropDown";

export interface INavItem {
    title: string,
    link: string,
    id: number,
    badge?: string | number | null
}

interface IProps {
    onClose: any,
    onOpen: any,

    isOnProfile: boolean,
    setIsOnProfile: any,

    onChangeLang: any,
    setIsOnLang: any,

    isOnLang: boolean,
    show: boolean,
    closeMobile: boolean,
    pathname: string,
    user: UserModel,
    NavItems: INavItem[]
}

export const flags = [
    {
        name: "EN",
        link: 'en-US'
    },
    {
        name: "GE",
        link: 'de-GE'
    },
];

const Sidebar = (ownProps: IProps) => {
    const marketDetail: MarketModel | null = getLocalStorage('marketDetail');
    const userFullDetails: UserFullDetailsModel | null = getLocalStorage('userFullDetails');

    const language = localStorage.getItem('i18nextLng') || 'en-US';
    const {t} = useTranslation();

    return (
        <Suspense fallback={<Loading/>}>
            <aside id="sidebar"
                   className={
                       `fixed block overflow-x-hidden without-scroll-bar 
                         overflow-y-auto m-0 p-0 clearfix sidebar 
                         ${ownProps.show ? 'open' : 'short'}
                         ${ownProps.closeMobile ? 'closeMobile' : null}
                         `
                   }>

                <div className="sidebar-container">
                    <div className="closeIcon ">
                        <img src="/assets/img/svg/icons/close-icon.svg" alt="" onClick={() => ownProps.onClose()}/>
                    </div>
                    <div className="user-details">
                        <div className="flex justify-end">
                            <div className="notifications-dropdown mx-3 relative ">
                                <div className="notifications-dropdown--button cursor-pointer"
                                     onClick={() => null}>
                                    {
                                        0 > 0
                                        && <div
                                            className="notifications-dropdown--badge flex items-center justify-center">{0}</div>
                                    }
                                    <img src={"/assets/img/svg/icons/notification-icon.svg"}
                                         alt={`${0} notifications`}/>
                                </div>
                                {/*{*/}
                                {/*    false &&*/}
                                {/*    <DropDown classes="notifications-dropdown--menu" items={[]}*/}
                                {/*              onChangeLang={(data: string) => console.log("")}*/}
                                {/*              kind="notifications"*/}
                                {/*              onChangeDropDown={(data: boolean) => Props.setIsOnNotification(data)}/>*/}
                                {/*}*/}
                            </div>
                            <div className="languages-dropdown ml-3 relative ">
                                <div className="languages-dropdown--button rounded-full cursor-pointer h-6 w-6"
                                     onClick={() => ownProps.setIsOnLang(!ownProps.isOnLang)}>

                                    {language === "en-US" &&
                                    <span className="w-full h-full flex">
                                        EN
                                        <img src="/assets/img/svg/icons/Polygon 3.svg" className="ml-1" alt=""/>
                                    </span>

                                    }
                                    {language === "de-GE" && <span className="w-full h-full flex">
                                        GE
                                        <img src="/assets/img/svg/icons/Polygon 3.svg" className="ml-1" alt=""/>
                                    </span>}
                                </div>
                                {
                                    ownProps.isOnLang && <DropDown classes="languages-dropdown--menu" items={flags}
                                                                   kind="languages"
                                                                   onChangeLang={(data: string) => ownProps.onChangeLang(data)}
                                                                   onChangeDropDown={(data: boolean) => ownProps.setIsOnLang(false)}/>
                                }
                            </div>
                        </div>

                        <ProfileDropDown isOnProfile={ownProps.isOnProfile}
                                         setIsOnProfile={(value: boolean) => ownProps.setIsOnProfile(value)}
                                         user={ownProps.user}/>
                    </div>

                    <div className="pt-4 md:pt-8 px-4 block ">
                        <Link to={URLs.dashboard_calculator}
                              className="flex flex-nowrap items-center ml-2 mr-5 justify-start smart-calculator-btn pl-6 h-12 bg-white rounded-full">
                            <span><img src="/assets/img/svg/icons/ic_date.svg" alt=""/></span>
                            <span className="ml-3 text-darkBlue">
                                {t('sidebar.smart_calculator')}
                            </span>
                        </Link>
                    </div>
                    <div className="pt-4 px-4 block ">
                        <div className="flex flex-nowrap items-center mx-2 justify-start pl-2 h-12  rounded-full">
                            <span><img src="/assets/img/svg/icons/calculator-blue.svg" alt=""/></span>
                            <span className="ml-3 text-sm">
                                Start Date: {userFullDetails ? moment(userFullDetails.user.startedAt).format('LL') : "-"}
                            </span>
                        </div>
                    </div>

                    <div className="pt-4 px-8 flex justify-between ">
                        <div><h5 className="font-bold">
                            {userFullDetails ? userFullDetails.level.displayTitle : "-"}
                        </h5></div>
                        <div>
                            <Link to={URLs.dashboard_accounts} className="flex items-center pt-1">
                                <span className="text-sm text-gray-600">
                                Accounts
                                </span>
                                <span className="ml-2">
                                    <img src="/assets/img/svg/icons/angle-gray.svg" alt=""/>
                                </span>
                            </Link>
                        </div>
                    </div>

                    <div
                        className="w-10/12 mt-4 border-gray-300 border-b-2 mx-auto pb-4 flex items-start justify-start flex-wrap">
                        {
                            userFullDetails && userFullDetails.level && userFullDetails.level.resources
                                ? userFullDetails.level.resources.map(item =>
                                    <div key={item.title}
                                         className="bg-pattensBlue  flex items-center justify-end mb-2 mr-2 w-auto h-8 px-3 rounded-full">
                                        <img src={item.icon2} className="w-3 svg-to-dodglerBlue" alt=""/>
                                        <span className="text-dodglerBlue text-xs ml-1">{item.title}</span>
                                    </div>
                                )
                                : ""
                        }


                    </div>

                    {
                        ownProps.pathname === "dashboard/home"
                            ?
                            <>
                                <div className="mt-5">
                                    <TotalDeposit classes="bg-blue-500 mt-3 mx-auto light"
                                                  title={t('home.total_balance')}
                                                  icon={"/assets/img/svg/icons/total_balance_icon_black.svg"}
                                                  badgeTitle={t('home.total_tokens')}
                                                  badge={marketDetail ? readableNumber(marketDetail.totalInvestToken) : "-"}
                                                  link={URLs.dashboard_transaction_history}
                                                  linkTitle={"Transactions"}
                                                  value={userFullDetails ? currencyFormatter(userFullDetails.financial.totalBalance) : "-"}
                                                  styles={{width: "290px", backgroundColor: "white"}}
                                    />
                                </div>
                                <div className="mt-3">
                                    <InfoCart
                                        value={marketDetail ? currencyFormatter(marketDetail.smbPrice) : "-"}
                                        title={t('home.token_price')}
                                        link={URLs.dashboard_accounts}
                                        icon={"/assets/img/svg/icons/smb-token-price.svg"}/>
                                </div>
                                <div className="mt-3">
                                    <InfoCart
                                        value={marketDetail ? currencyFormatter(marketDetail.marketCap) : "-"}
                                        title={t('home.market_cap')}
                                        link={URLs.dashboard_accounts}
                                        icon={"/assets/img/svg/icons/market-cap-icon.svg"}/>
                                </div>

                            </>
                            :
                            <>
                                <div className="mt-3">
                                    <InfoCart
                                        value={userFullDetails ? currencyFormatter(userFullDetails.financial.totalProfit) : "-"}
                                        title={t('home.total_profit')}
                                        link={URLs.dashboard_accounts}
                                        icon={"/assets/img/svg/icons/smb-token-price.svg"}/>
                                </div>
                                <div className="mt-3">
                                    <InfoCart
                                        value={userFullDetails ? currencyFormatter(userFullDetails.financial.totalBalance) : "-"}
                                        title={t('home.total_balance')}
                                        link={URLs.dashboard_accounts}
                                        icon={"/assets/img/svg/icons/total_balance_icon_black.svg"}/>
                                </div>
                                <div className="mt-3">
                                    <InfoCart
                                        value={userFullDetails ? currencyFormatter(userFullDetails.financial.totalDeposit) : "-"}
                                        title={t('home.total_deposit')}
                                        link={URLs.dashboard_accounts}
                                        icon={"/assets/img/svg/icons/market-cap-icon.svg"}/>
                                </div>

                            </>
                    }


                    <div className="mt-16">
                        <img src={"/assets/img/svg/upgradeVector.svg"} alt="" className="mx-auto block"/>
                    </div>

                </div>
            </aside>
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        user: state.auth.user
    }
};

export default connect(mapStateToProps)(Sidebar);

