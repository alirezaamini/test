import * as React from 'react';
import './header.scss';
import {Suspense, useEffect, useState} from "react";
import URLs from "../../../constants/URLs";
import {connect} from "react-redux";
import DropDown from "../../../components/DropDowns/DropDown";
import UserModel from "../../../models/user.model";
import Loading from "../../../components/UiKits/Loading/Loading";
import {checkAvatar, handleAvatarError} from "../../../assets/helpers/utilities";
import {useTranslation} from "react-i18next";
import {fetchNotifications} from "../../../actions/auth";
import {getLocalStorage} from "../../../assets/helpers/storage";
import MembershipModel from "../../../models/membership.model";
import {flags} from "../Sidebar/Sidebar";
import {ProfileDropDown} from "./ProfileDropDown";

interface IProps {
    pathname: string,
    onOpen: any,
    onChangeLang: any,
    fetchNotifications: any,
    onClose: any,
    isOpen: boolean,
    isClose: boolean,
    setIsOnLang: any,
    setIsOnNotification: any,
    isOnNotification: boolean,
    isOnLang: boolean,
    user: UserModel,
    notifications: string[],
    notificationsLoading: boolean,
    notificationsError: string,
    newNotifications: number,

    isOnProfile: boolean,
    setIsOnProfile: any
}

export interface IFlag {
    name: string,
    link: string,
}


const Header = (Props: IProps) => {
    const memberships: MembershipModel[] = getLocalStorage('memberships') || [];
    const userData: UserModel | null = getLocalStorage('userData') || null;

    const fetchNotifications = () => {
        Props.setIsOnLang(false);
        Props.setIsOnNotification(!Props.isOnNotification);

        //fetch notifications
        Props.fetchNotifications({take: 5, skip: 0}, true)
    };


    const {t} = useTranslation();
    useEffect(() => {


    }, [Props.pathname]);


    const language = localStorage.getItem('i18nextLng') || 'en-US';

    return (
        <Suspense fallback={<Loading/>}>
            <div className="main-header">
                <div
                    className="main-header-container flex justify-between align-middle items-center sm:px-3 md:px-8 xl:px-16 shadow small-px-6">

                    <div className="main-header-container--left h-full inline-flex">
                        <div className="inline-flex items-center mr-3 xl:mr-10 h-16 md:h-full relative">
                            <div className="block lg:hidden pb-4 mt-4">
                                <ProfileDropDown isOnProfile={Props.isOnProfile}
                                                 setIsOnProfile={(value: boolean) => Props.setIsOnProfile(value)}
                                                 user={Props.user}/>
                            </div>
                            <div className="hidden lg-block">
                                <img src={"/assets/img/svg/logo-typography.svg"} className="logo" alt=""/>
                            </div>
                        </div>

                        {
                            memberships.length === 2
                            &&
                            <div className="dashboard-buttons inline-flex items-center h-full">
                                {
                                    userData && userData.registerMembership === "arbitrage-investor"
                                        ?
                                        <>
                                            <a href={"/dashboard/home"}
                                               className={`inline-flex items-center justify-center py-2 ${Props.pathname.includes('/dashboard') && "active"}`}>
                                                Arbitrage Dashboard
                                            </a>
                                            <a href={"/token-dashboard/home"}
                                               className={`ml-3 inline-flex items-center justify-center py-2 ${Props.pathname.includes('/token-dashboard') && "active"}`}>
                                                {t('header.token_dashboard')}
                                            </a>
                                        </>
                                        :
                                        <>
                                            <a href={"/token-dashboard/home"}
                                               className={`inline-flex items-center justify-center py-2 ${Props.pathname.includes('/token-dashboard') && "active"}`}>
                                                {t('header.token_dashboard')}
                                            </a>
                                            <a href={"/dashboard/home"}
                                               className={`ml-3 inline-flex items-center justify-center py-2 ${Props.pathname.includes('/dashboard') && "active"}`}>
                                                Arbitrage Dashboard
                                            </a>
                                        </>
                                }

                            </div>

                        }


                        <div className="mobile">

                            <div className="user-details">
                                <div className="flex justify-end">
                                    <div className="notifications-dropdown mx-3 relative ">
                                        <div className="notifications-dropdown--button cursor-pointer"
                                             onClick={() => null}>
                                            {
                                                0 > 0
                                                && <div
                                                    className="notifications-dropdown--badge flex items-center justify-center">{0}</div>
                                            }
                                            <img src={"/assets/img/svg/icons/notification-icon.svg"}
                                                 alt={`${0} notifications`}/>
                                        </div>
                                        {/*{*/}
                                        {/*    false &&*/}
                                        {/*    <DropDown classes="notifications-dropdown--menu" items={[]}*/}
                                        {/*              onChangeLang={(data: string) => console.log("")}*/}
                                        {/*              kind="notifications"*/}
                                        {/*              onChangeDropDown={(data: boolean) => Props.setIsOnNotification(data)}/>*/}
                                        {/*}*/}
                                    </div>
                                    <div className="languages-dropdown ml-3 relative ">
                                        <div className="languages-dropdown--button rounded-full cursor-pointer h-6 w-6"
                                             onClick={() => Props.setIsOnLang(!Props.isOnLang)}>

                                            {language === "en-US" &&
                                            <span className="w-full h-full flex">
                                        EN
                                        <img src="/assets/img/svg/icons/Polygon 3.svg" className="ml-1" alt=""/>
                                    </span>

                                            }
                                            {language === "de-GE" && <span className="w-full h-full flex">
                                        GE
                                        <img src="/assets/img/svg/icons/Polygon 3.svg" className="ml-1" alt=""/>
                                    </span>}
                                        </div>
                                        {
                                            Props.isOnLang && <DropDown classes="languages-dropdown--menu" items={flags}
                                                                        kind="languages"
                                                                        onChangeLang={(data: string) => Props.onChangeLang(data)}
                                                                        onChangeDropDown={(data: boolean) => Props.setIsOnLang(data)}/>
                                        }

                                    </div>
                                </div>

                            </div>
                            <div onClick={() => Props.onClose()}
                                 className={`hamburger-menu inline-flex flex-wrap justify-center items-center
                                        ${!Props.isClose && 'opened'}`}>
                                <div/>
                                <div/>
                                <div/>
                            </div>

                        </div>

                    </div>

                    <div className="flex flex-no-wrap justify-start main-header-container--right">
                        <div className="search relative mr-2 xl:mr-12">
                            <input type="search" className="w-full" placeholder={t('header.type_something')}/>
                        </div>
                    </div>
                </div>
            </div>
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        user: state.auth.user,
        notifications: state.auth.notifications.data,
        notificationsLoading: state.auth.notifications.loading,
        notificationsError: state.auth.notifications.error,
        newNotifications: state.auth.notifications.new
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchNotifications: (data: { take: number, skip: number }, reset: boolean) => dispatch(fetchNotifications(data, reset))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
