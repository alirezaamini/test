import React from "react";
import URLs from "../constants/URLs";

//layout
const DashboardWrapper = React.lazy(() => import('../layouts/dashboard/DashboardWrapper'));
const TokenDashboardWrapper = React.lazy(() => import('../layouts/dashboard/TokenDashboardWrapper'));

//Views
const My404Component = React.lazy(() => import('../containers/404'));
const GetPremium = React.lazy(() => import('../containers/GetPremium/GetPremium'));

const Home = React.lazy(() => import('../views/Dashboard/Home/Index'));
const LiveArbitrage = React.lazy(() => import('../views/Dashboard/LiveArbitrage/Index'));
const Accounts = React.lazy(() => import('../views/Dashboard/Accounts/Index'));
const ReferralProgram = React.lazy(() => import('../views/Dashboard/ReferralProgram/Index'));
const AccountsDetails = React.lazy(() => import('../views/Dashboard/AccountDetails/Index'));
const Settings = React.lazy(() => import('../views/Dashboard/Settings/Index'));
const FAQ = React.lazy(() => import('../views/Dashboard/FAQ/Index'));
const Documents = React.lazy(() => import('../views/Dashboard/Documents/Index'));
const TransactionHistory = React.lazy(() => import('../views/Dashboard/TransactionHistory/Index'));
const KYC = React.lazy(() => import('../views/Dashboard/KYC/Index'));
const Withdrawal = React.lazy(() => import('../views/Dashboard/WithDrawal/Index'));
const Invest = React.lazy(() => import('../views/Dashboard/Invest/Index'));
const Calculator = React.lazy(() => import('../views/Dashboard/Calculator/Index'));


//dashboard token
const TokenHome = React.lazy(() => import('../views/TokenDashboard/Home/Index'));
const TokenTransactionHistory = React.lazy(() => import('../views/TokenDashboard/TransactionHistory/Index'));
const TokenGetPremium = React.lazy(() => import('../containers/GetPremium/GetPremium'));
const TokenAccountsDetails = React.lazy(() => import('../views/TokenDashboard/AccountDetails/Index'));
const TokenKYC = React.lazy(() => import('../views/TokenDashboard/KYC/Index'));
const TokenSettings = React.lazy(() => import('../views/TokenDashboard/Settings/Index'));

export const RouterSwitch = (membership: string) => {
    switch (membership) {
        case "arbitrage-investor":
            return [
                {
                    path: URLs.dashboard_home,
                    component: Home
                },
                {
                    path: URLs.dashboard_live_arbitrage,
                    component: LiveArbitrage
                },
                {
                    path: URLs.dashboard_accounts,
                    component: Accounts
                },
                {
                    path: URLs.dashboard_account_details,
                    component: AccountsDetails
                },
                {
                    path: URLs.dashboard_transaction_history,
                    component: TransactionHistory
                },
                {
                    path: URLs.dashboard_withdrawal,
                    component: Withdrawal
                },
                {
                    path: URLs.dashboard_invest,
                    component: Invest
                },
                {
                    path: URLs.dashboard_calculator,
                    component: Calculator
                },
                {
                    path: URLs.dashboard_kyc,
                    component: KYC
                },
                {
                    path: URLs.dashboard_settings,
                    component: Settings
                },
                {
                    path: URLs.dashboard_referral_program,
                    component: ReferralProgram
                },
                {
                    path: URLs.dashboard_documents,
                    component: Documents
                },
                {
                    path: URLs.dashboard_faq,
                    component: FAQ
                },
                {
                    path: URLs.dashboard_get_premium,
                    component: GetPremium
                },
                {
                    component: My404Component
                },
            ]
        case "token-investor":
            return [
                {
                    path: URLs.token_dashboard_home,
                    component: TokenHome
                },
                {
                    path: URLs.token_dashboard_account_details,
                    component: TokenAccountsDetails
                },
                {
                    path: URLs.token_dashboard_transaction_history,
                    component: TokenTransactionHistory
                },
                {
                    path: URLs.token_dashboard_kyc,
                    component: TokenKYC
                },
                {
                    path: URLs.token_dashboard_settings,
                    component: TokenSettings
                },
                {
                    path: URLs.token_dashboard_get_premium,
                    component: TokenGetPremium
                },
                {
                    component: My404Component
                },
            ]
        default:
            return [
                {
                    path: URLs.dashboard_account_details,
                    component: AccountsDetails
                },
                {
                    path: URLs.dashboard_kyc,
                    component: KYC
                },
                {
                    path: URLs.dashboard_settings,
                    component: Settings
                },
                {
                    path: URLs.dashboard_referral_program,
                    component: ReferralProgram
                },
                {
                    path: URLs.dashboard_documents,
                    component: Documents
                },
                {
                    path: URLs.dashboard_faq,
                    component: FAQ
                },
                {
                    path: URLs.dashboard_get_premium,
                    component: GetPremium
                },
                {
                    component: My404Component
                },
            ]
    }
}


export const LayoutSwitch = (membership: string) => {
    switch (membership) {
        case "token-investor":
            return TokenDashboardWrapper
        case "arbitrage-investor":
            return DashboardWrapper
        default:
            return
    }
}

export const PathSwitch = (membership: string) => {
    switch (membership) {
        case "token-investor":
            return "/token-dashboard"
        case "arbitrage-investor":
            return "/dashboard"
        default:
            return
    }
}